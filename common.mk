COMMON_FLAGS    = -Wall -Werror
CFLAGS          += $(COMMON_FLAGS)
CXXFLAGS        += $(COMMON_FLAGS) -std=c++14

ifneq ($(DEBUG),)
  CFLAGS        += -g
  CXXFLAGS      += -g
else
  CFLAGS        += -DNDEBUG
  CXXFLAGS      += -DNDEBUG
endif


ifneq ($(CPPUTEST_HOME),)
  CPPUTEST_FLAGS        = -I$(CPPUTEST_HOME)/include
  CPPUTEST_LDFLAGS      = -L$(CPPUTEST_HOME)/lib -lCppUTest -lCppUTestExt
  HAS_CPPUTEST          = 1
else
  HAS_CPPUTEST          = $(shell pkg-config cpputest && echo 1)
  ifeq ($(HAS_CPPUTEST),1)
    CPPUTEST_FLAGS      += $(shell pkg-config --cflags cpputest 2>/dev/null)
    CPPUTEST_LDFLAGS    += $(shell pkg-config --libs cpputest 2>/dev/null)
  endif
endif

# AVR compiler
AVR_CROSS_COMPILE ?= avr-
MCU               = atmega328p
AVR_LDFLAGS       =
AVR_CFLAGS        += $(CFLAGS)
AVR_CFLAGS        += -mmcu=$(MCU) -Os -Wall -Wextra -DF_CPU=16000000 -I.
AVR_CC            = $(AVR_CROSS_COMPILE)gcc
AVR_LD            = $(AVR_CROSS_COMPILE)ld
AVR_OBJCPY        = $(AVR_CROSS_COMPILE)objcopy
HAS_AVR_CC        = $(shell $(AVR_CC) --version >/dev/null 2>&1 && echo 1)

# Silence mode
ifneq ($(V),)
  SILENCE       =
else
  SILENCE       = @
endif

PRINTF_CMD_FMT  ="%-15s%s%s\n"
