/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>

#include "../src/portamento.h"
#include "../src/portamento_private.h"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

//------------------------------------------------------------------------------
TEST_GROUP(portamento)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }
};

//------------------------------------------------------------------------------
TEST(portamento, releaseNotOnVoice3)
{
  uint8_t midiNote = 0xAB;

  portaStatus.voiceStatus[2].currentNote = midiNote;

  mock().expectOneCall("SID_releaseVoice").
      withParameter("voice", RS_VOICE_3).
      andReturnValue(0);

  CHECK_EQUAL(0, PORTA_releaseNote(RS_VOICE_3, midiNote));;

  mock().checkExpectations();

  CHECK_EQUAL(0, portaStatus.voiceStatus[2].currentNote);
}

//------------------------------------------------------------------------------
TEST(portamento, releaseNotOnVoice2)
{
  uint8_t midiNote = 0xAB;

  portaStatus.voiceStatus[1].currentNote = midiNote;

  mock().expectOneCall("SID_releaseVoice").
      withParameter("voice", RS_VOICE_2).
      andReturnValue(0);

  CHECK_EQUAL(0, PORTA_releaseNote(RS_VOICE_2, midiNote));;

  mock().checkExpectations();

  CHECK_EQUAL(0, portaStatus.voiceStatus[1].currentNote);
}

//------------------------------------------------------------------------------
TEST(portamento, releaseNotOnVoice1)
{
  uint8_t midiNote = 0xAB;

  portaStatus.voiceStatus[0].currentNote = midiNote;

  mock().expectOneCall("SID_releaseVoice").
      withParameter("voice", RS_VOICE_1).
      andReturnValue(0);

  CHECK_EQUAL(0, PORTA_releaseNote(RS_VOICE_1, midiNote));;

  mock().checkExpectations();

  CHECK_EQUAL(0, portaStatus.voiceStatus[0].currentNote);
}

//------------------------------------------------------------------------------
TEST(portamento, releaseNotOnVoice1CannotReleaseInSid)
{
  uint8_t midiNote = 0xAB;

  portaStatus.voiceStatus[0].currentNote = midiNote;

  mock().expectOneCall("SID_releaseVoice").
      withParameter("voice", RS_VOICE_1).
      andReturnValue(-1);

  CHECK_EQUAL(-1, PORTA_releaseNote(RS_VOICE_1, midiNote));;

  mock().checkExpectations();

  CHECK_EQUAL(0, portaStatus.voiceStatus[0].currentNote);
}

//------------------------------------------------------------------------------
TEST(portamento, releaseNotOnVoice1NotLastPlayedNote)
{
  uint8_t midiNote = 0xAB;

  portaStatus.voiceStatus[0].currentNote = 0;

  CHECK_EQUAL(0, PORTA_releaseNote(RS_VOICE_1, midiNote));
}

//------------------------------------------------------------------------------
TEST(portamento, startNoteOnVoice3)
{
  uint8_t midiNote = 0xAB;
  uint16_t sidFreq = 0x1234;

  portaStatus.voiceStatus[2].currentNote = 0;

  mock().expectOneCall("SIDFREQ_midiToSidFreq").
      withParameter("midi", midiNote).
      withOutputParameterReturning("sidFreq", &sidFreq, sizeof(sidFreq)).
      andReturnValue(0);
  mock().expectOneCall("SID_setVoiceFrequency").
      withParameter("voice", RS_VOICE_3).
      withParameter("frequency", sidFreq).
      andReturnValue(0);
  mock().expectOneCall("SID_startVoice").
      withParameter("voice", RS_VOICE_3).
      andReturnValue(0);

  CHECK_EQUAL(0, PORTA_startNote(RS_VOICE_3, midiNote));

  mock().checkExpectations();

  CHECK_EQUAL(midiNote, portaStatus.voiceStatus[2].currentNote);
}

//------------------------------------------------------------------------------
TEST(portamento, startNoteOnVoice2)
{
  uint8_t midiNote = 0xAB;
  uint16_t sidFreq = 0x1234;

  portaStatus.voiceStatus[1].currentNote = 0;

  mock().expectOneCall("SIDFREQ_midiToSidFreq").
      withParameter("midi", midiNote).
      withOutputParameterReturning("sidFreq", &sidFreq, sizeof(sidFreq)).
      andReturnValue(0);
  mock().expectOneCall("SID_setVoiceFrequency").
      withParameter("voice", RS_VOICE_2).
      withParameter("frequency", sidFreq).
      andReturnValue(0);
  mock().expectOneCall("SID_startVoice").
      withParameter("voice", RS_VOICE_2).
      andReturnValue(0);

  CHECK_EQUAL(0, PORTA_startNote(RS_VOICE_2, midiNote));

  mock().checkExpectations();

  CHECK_EQUAL(midiNote, portaStatus.voiceStatus[1].currentNote);
}

//------------------------------------------------------------------------------
TEST(portamento, startNoteOnVoice1)
{
  uint8_t midiNote = 0xAB;
  uint16_t sidFreq = 0x1234;

  portaStatus.voiceStatus[1].currentNote = 0;

  mock().expectOneCall("SIDFREQ_midiToSidFreq").
      withParameter("midi", midiNote).
      withOutputParameterReturning("sidFreq", &sidFreq, sizeof(sidFreq)).
      andReturnValue(0);
  mock().expectOneCall("SID_setVoiceFrequency").
      withParameter("voice", RS_VOICE_1).
      withParameter("frequency", sidFreq).
      andReturnValue(0);
  mock().expectOneCall("SID_startVoice").
      withParameter("voice", RS_VOICE_1).
      andReturnValue(0);

  CHECK_EQUAL(0, PORTA_startNote(RS_VOICE_1, midiNote));

  mock().checkExpectations();

  CHECK_EQUAL(midiNote, portaStatus.voiceStatus[0].currentNote);
}

//------------------------------------------------------------------------------
TEST(portamento, startNoteOnVoice1CannotStartVoice)
{
  uint8_t midiNote = 0xAB;
  uint16_t sidFreq = 0x1234;

  mock().expectOneCall("SIDFREQ_midiToSidFreq").
      withParameter("midi", midiNote).
      withOutputParameterReturning("sidFreq", &sidFreq, sizeof(sidFreq)).
      andReturnValue(0);
  mock().expectOneCall("SID_setVoiceFrequency").
      withParameter("voice", RS_VOICE_1).
      withParameter("frequency", sidFreq).
      andReturnValue(0);
  mock().expectOneCall("SID_startVoice").
      withParameter("voice", RS_VOICE_1).
      andReturnValue(-1);

  CHECK_EQUAL(-1, PORTA_startNote(RS_VOICE_1, midiNote));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(portamento, startNoteOnVoice1CannotWriteFreqencie)
{
  uint8_t midiNote = 0xAB;
  uint16_t sidFreq = 0x1234;

  mock().expectOneCall("SIDFREQ_midiToSidFreq").
      withParameter("midi", midiNote).
      withOutputParameterReturning("sidFreq", &sidFreq, sizeof(sidFreq)).
      andReturnValue(0);
  mock().expectOneCall("SID_setVoiceFrequency").
      withParameter("voice", RS_VOICE_1).
      withParameter("frequency", sidFreq).
      andReturnValue(-1);

  CHECK_EQUAL(-1, PORTA_startNote(RS_VOICE_1, midiNote));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(portamento, startNoteOnVoice1CannotConvertNote)
{
  uint8_t midiNote = 0xAB;
  uint16_t sidFreq = 0x1234;

  mock().expectOneCall("SIDFREQ_midiToSidFreq").
      withParameter("midi", midiNote).
      withOutputParameterReturning("sidFreq", &sidFreq, sizeof(sidFreq)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, PORTA_startNote(RS_VOICE_1, midiNote));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(portamento, initPortaInfoSetToZero)
{
  PortaStatus expectedPortaStatus;

  std::memset(&expectedPortaStatus, 0x0, sizeof(PortaStatus));
  std::memset(&portaStatus, 0xA, sizeof(PortaStatus));

  PORTA_init();

  MEMCMP_EQUAL(&expectedPortaStatus, &portaStatus, sizeof(portaStatus));
}

//------------------------------------------------------------------------------
TEST(portamento, initSuccess)
{
  CHECK_EQUAL(0, PORTA_init());
}
