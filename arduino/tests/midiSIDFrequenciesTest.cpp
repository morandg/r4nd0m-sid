/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>

#include "../src/midi_SID_freq_table.h"
#include "../src/midi_SID_frequencies.h"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

// Predictable MIDI to frequency lookup table
MidiFreqRef SID_freq_table[NOTES_PER_OCTAVES];
static const uint8_t firstMidiNote = 50;

//------------------------------------------------------------------------------
TEST_GROUP(MidiSIDFrequencies)
{
  TEST_SETUP()
  {
    uint8_t currentMidiNote = firstMidiNote;

    memset(SID_freq_table, 0, sizeof(SID_freq_table));
    for(unsigned int i = 0 ; i < NOTES_PER_OCTAVES ; ++i)
    {
      SID_freq_table[i].midiNote = currentMidiNote++;
    }
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(MidiSIDFrequencies, returnHigherOctaveFromLastIndex)
{
  uint8_t midiNote = SID_freq_table[NOTES_PER_OCTAVES - 1].midiNote;
  uint16_t frequency = 0x4000;
  uint16_t frequencyOut;

  midiNote += NOTES_PER_OCTAVES;
  SID_freq_table[NOTES_PER_OCTAVES - 1].frequency = frequency;

  CHECK_EQUAL(0, SIDFREQ_midiToSidFreq(midiNote, &frequencyOut));
  CHECK_EQUAL(frequency * 2, frequencyOut);
}

//------------------------------------------------------------------------------
TEST(MidiSIDFrequencies, returnHigherOctaveEqualOverflow)
{
  uint8_t midiNote = SID_freq_table[0].midiNote;
  uint16_t frequency = 0x4000;
  uint16_t frequencyOut;

  midiNote += NOTES_PER_OCTAVES;
  SID_freq_table[0].frequency = frequency;

  CHECK_EQUAL(0, SIDFREQ_midiToSidFreq(midiNote, &frequencyOut));
  CHECK_EQUAL(frequency * 2, frequencyOut);
}

//------------------------------------------------------------------------------
TEST(MidiSIDFrequencies, returnHigherOctaveFreqOverflow)
{
  uint8_t midiNote = SID_freq_table[NOTES_PER_OCTAVES - 1].midiNote;
  uint16_t frequency = 0x8000;
  uint16_t frequencyOut;

  midiNote += NOTES_PER_OCTAVES;
  SID_freq_table[NOTES_PER_OCTAVES - 1].frequency = frequency;

  CHECK_EQUAL(-1, SIDFREQ_midiToSidFreq(midiNote, &frequencyOut));
  CHECK_EQUAL(0, frequencyOut);
}

//------------------------------------------------------------------------------
TEST(MidiSIDFrequencies, returnHigherOctaveFreq)
{
  uint8_t midiNote = SID_freq_table[NOTES_PER_OCTAVES - 1].midiNote;
  uint16_t frequency = 0x2000;
  uint16_t frequencyOut;

  midiNote += NOTES_PER_OCTAVES;
  SID_freq_table[NOTES_PER_OCTAVES - 1].frequency = frequency;

  CHECK_EQUAL(0, SIDFREQ_midiToSidFreq(midiNote, &frequencyOut));
  CHECK_EQUAL(frequency * 2, frequencyOut);
}

//------------------------------------------------------------------------------
TEST(MidiSIDFrequencies, returnLowerOctaveFreq)
{
  uint8_t midiNote = SID_freq_table[0].midiNote;
  uint16_t frequency = 0xABCD;
  uint16_t frequencyOut;

  midiNote -= NOTES_PER_OCTAVES;
  SID_freq_table[0].frequency = frequency;

  CHECK_EQUAL(0, SIDFREQ_midiToSidFreq(midiNote, &frequencyOut));
  CHECK_EQUAL(frequency / 2, frequencyOut);
}

//------------------------------------------------------------------------------
TEST(MidiSIDFrequencies, returnLastMidiNote)
{
  uint8_t midiNote = SID_freq_table[NOTES_PER_OCTAVES - 1].midiNote;
  uint16_t frequency = 0xABCD;
  uint16_t frequencyOut;

  SID_freq_table[NOTES_PER_OCTAVES - 1].frequency = frequency;

  CHECK_EQUAL(0, SIDFREQ_midiToSidFreq(midiNote, &frequencyOut));
  CHECK_EQUAL(frequency, frequencyOut);
}

//------------------------------------------------------------------------------
TEST(MidiSIDFrequencies, returnFirstMidiNote)
{
  uint8_t midiNote = SID_freq_table[0].midiNote;
  uint16_t frequency = 0xABCD;
  uint16_t frequencyOut;

  SID_freq_table[0].frequency = frequency;

  CHECK_EQUAL(0, SIDFREQ_midiToSidFreq(midiNote, &frequencyOut));
  CHECK_EQUAL(frequency, frequencyOut);
}
