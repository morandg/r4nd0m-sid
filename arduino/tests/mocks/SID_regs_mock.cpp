/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../../src/SID_regs.h"

#include <CppUTestExt/MockSupport.h>

//------------------------------------------------------------------------------
int SIDREGS_init()
{
  return mock().actualCall(__func__).
      returnIntValue();
}

//------------------------------------------------------------------------------
int SIDREGS_readRegister(uint8_t reg, uint8_t* value)
{
  return mock().actualCall(__func__).
      withParameter("reg", reg).
      withOutputParameter("value", value).
      returnIntValue();
}

//------------------------------------------------------------------------------
int SIDREGS_writeRegister(uint8_t reg, uint8_t value)
{
  return mock().actualCall(__func__).
      withParameter("reg", reg).
      withParameter("value", value).
      returnIntValue();
}
