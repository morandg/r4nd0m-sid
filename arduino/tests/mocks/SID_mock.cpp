/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../../src/portamento.h"

#include <CppUTestExt/MockSupport.h>

//------------------------------------------------------------------------------
int SID_init()
{
  return mock().actualCall(__func__).
      returnIntValue();
}

//------------------------------------------------------------------------------
int SID_setVoiceShape(enum RSVoice_t voice, uint8_t shape)
{
  return mock().actualCall(__func__).
      withParameter("voice", voice).
      withParameter("shape", shape).
      returnIntValue();
}

//------------------------------------------------------------------------------
int SID_setVoiceFrequency(enum RSVoice_t voice,  uint16_t frequency)
{
  return mock().actualCall(__func__).
      withParameter("voice", voice).
      withParameter("frequency", frequency).
      returnIntValue();
}

//------------------------------------------------------------------------------
int SID_startVoice(enum RSVoice_t voice)
{
  return mock().actualCall(__func__).
      withParameter("voice", voice).
      returnIntValue();
}

//------------------------------------------------------------------------------
int SID_releaseVoice(enum RSVoice_t voice)
{
  return mock().actualCall(__func__).
      withParameter("voice", voice).
      returnIntValue();
}
