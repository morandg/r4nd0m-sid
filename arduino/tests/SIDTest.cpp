/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/SID.h"
#include "../src/SID_regs.h"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

//------------------------------------------------------------------------------
TEST_GROUP(SID)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }
};

//------------------------------------------------------------------------------
TEST(SID, setPulseWidthVoice1InvalidPulse)
{
  uint16_t pulseWidth = 0x1FFF;

  CHECK_EQUAL(-1, SID_setPulseWidth(RS_VOICE_3, pulseWidth));
}

//------------------------------------------------------------------------------
TEST(SID, setPulseWidthVoice3)
{
  uint16_t pulseWidth = 0x0FFF;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x10).
      withParameter("value", 0xFF).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x11).
      withParameter("value", 0x0F).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setPulseWidth(RS_VOICE_3, pulseWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulseWidthVoice2)
{
  uint16_t pulseWidth = 0x0FFF;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x09).
      withParameter("value", 0xFF).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x0A).
      withParameter("value", 0x0F).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setPulseWidth(RS_VOICE_2, pulseWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulseWidthVoice1)
{
  uint16_t pulseWidth = 0x0FFF;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x02).
      withParameter("value", 0xFF).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x03).
      withParameter("value", 0x0F).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setPulseWidth(RS_VOICE_1, pulseWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulseWidthVoice1WriteHighFails)
{
  uint16_t pulseWidth = 0x0FFF;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x02).
      withParameter("value", 0xFF).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x03).
      withParameter("value", 0x0F).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setPulseWidth(RS_VOICE_1, pulseWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulseWidthVoice1WriteLowFails)
{
  uint16_t pulseWidth = 0x0FFF;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x02).
      withParameter("value", 0xFF).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setPulseWidth(RS_VOICE_1, pulseWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulseWidthInvalidVoice)
{
  CHECK_EQUAL(-1, SID_setPulseWidth((RSVoice_t)4, 0));
}

//------------------------------------------------------------------------------
TEST(SID, setSustainReleaseVoice3)
{
  uint8_t sustainRelease = 0xAB;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x14).
      withParameter("value", sustainRelease).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setSustainRelease(RS_VOICE_3, sustainRelease));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainReleaseVoice2)
{
  uint8_t sustainRelease = 0xAB;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x0D).
      withParameter("value", sustainRelease).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setSustainRelease(RS_VOICE_2, sustainRelease));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainReleaseVoice1)
{
  uint8_t sustainRelease = 0xAB;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x06).
      withParameter("value", sustainRelease).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setSustainRelease(RS_VOICE_1, sustainRelease));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainReleaseVoice1WriteFails)
{
  uint8_t sustainRelease = 0xAB;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x06).
      withParameter("value", sustainRelease).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setSustainRelease(RS_VOICE_1, sustainRelease));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainReleaseInvalidVoice)
{
  uint8_t sustainRelease = 0xAB;

  CHECK_EQUAL(-1, SID_setSustainRelease((RSVoice_t)4, sustainRelease));
}

//------------------------------------------------------------------------------
TEST(SID, setAttackDecayVoice3)
{
  uint8_t attackDecay = 0xAB;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x13).
      withParameter("value", attackDecay).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setAttackDecay(RS_VOICE_3, attackDecay));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setAttackDecayVoice2)
{
  uint8_t attackDecay = 0xAB;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x0C).
      withParameter("value", attackDecay).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setAttackDecay(RS_VOICE_2, attackDecay));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setAttackDecayVoice1)
{
  uint8_t attackDecay = 0xAB;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x05).
      withParameter("value", attackDecay).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setAttackDecay(RS_VOICE_1, attackDecay));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setAttackDecayVoice1WriteFails)
{
  uint8_t attackDecay = 0xAB;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x05).
      withParameter("value", attackDecay).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setAttackDecay(RS_VOICE_1, attackDecay));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setAttackDecayInvalidVoice)
{
  uint8_t attackDecay = 0xAB;

  CHECK_EQUAL(-1, SID_setAttackDecay((RSVoice_t)4, attackDecay));
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeSuccessRegister0xF0)
{
  uint8_t regValue = 0xF0;
  uint8_t volume = 0xF;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x18).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x18).
      withParameter("value", 0xFF).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVolume(volume));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeSuccess)
{
  uint8_t regValue = 0;
  uint8_t volume = 0x01;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x18).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x18).
      withParameter("value", 0x01).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVolume(volume));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeCannotWriteRegister)
{
  uint8_t regValue = 0;
  uint8_t volume = 0x01;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x18).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x18).
      withParameter("value", 0x01).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setVolume(volume));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeCannotReadRegister)
{
  uint8_t regValue = 0;
  uint8_t volume = 0x01;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x18).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setVolume(volume));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeInvalidVolume)
{
  uint8_t volume = 0x10;

  CHECK_EQUAL(-1, SID_setVolume(volume));
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice3)
{
  uint8_t regValue = 1;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x12).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x12).
      withParameter("value", 0).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_releaseVoice(RS_VOICE_3));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice2)
{
  uint8_t regValue = 1;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x0B).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x0B).
      withParameter("value", 0).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_releaseVoice(RS_VOICE_2));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice1RegValueFF)
{
  uint8_t regValue = 0xFF;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0xFE).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_releaseVoice(RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice1)
{
  uint8_t regValue = 0x01;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_releaseVoice(RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice1CannotWriteRegister)
{
  uint8_t regValue = 0x01;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_releaseVoice(RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice1CannotReadRegister)
{
  uint8_t regValue = 0x01;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_releaseVoice(RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoiceInvalidVoice)
{
  CHECK_EQUAL(-1, SID_releaseVoice((RSVoice_t)0xFF));
}

//------------------------------------------------------------------------------
TEST(SID, startVoice3)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x12).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x12).
      withParameter("value", regValue | 1).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_startVoice(RS_VOICE_3));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice2)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x0B).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x0B).
      withParameter("value", regValue | 1).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_startVoice(RS_VOICE_2));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice1RegValueFE)
{
  uint8_t regValue = 0xFE;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", regValue | 1).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_startVoice(RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice1)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 1).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_startVoice(RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice1CannotWriteRegister)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0x01).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_startVoice(RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice1CannotReadRegister)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_startVoice(RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoiceInvalidVoice)
{
  CHECK_EQUAL(-1, SID_startVoice((RSVoice_t)0xff));
}

//------------------------------------------------------------------------------
TEST(SID, setVoice3FrequencyABCD)
{
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x0E).
      withParameter("value", 0xCD).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x0F).
      withParameter("value", 0xAB).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceFrequency(RS_VOICE_3, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoice2FrequencyABCD)
{
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x07).
      withParameter("value", 0xCD).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x08).
      withParameter("value", 0xAB).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceFrequency(RS_VOICE_2, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoice1FrequencyABCD)
{
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x00).
      withParameter("value", 0xCD).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x01).
      withParameter("value", 0xAB).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceFrequency(RS_VOICE_1, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoice1FrequencyABCDCannotSetHighFreq)
{
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x00).
      withParameter("value", 0xCD).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x01).
      withParameter("value", 0xAB).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setVoiceFrequency(RS_VOICE_1, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoice1FrequencyABCDCannotSetLowFreq)
{
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x00).
      withParameter("value", 0xCD).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setVoiceFrequency(RS_VOICE_1, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoiceFrequencyInvalidVoice)
{
  RSSIDFrequency frequency = 0x0;

  CHECK_EQUAL(-1, SID_setVoiceShape((RSVoice_t)0xFF, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice3Noise)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x12).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x12).
      withParameter("value", 0x80).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceShape(RS_VOICE_3, RS_SHAPE_NOISE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice2Noise)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x0B).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x0B).
      withParameter("value", 0x80).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceShape(RS_VOICE_2, RS_SHAPE_NOISE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1Noise)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0x80).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceShape(RS_VOICE_1, RS_SHAPE_NOISE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1Pulse)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0x40).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceShape(RS_VOICE_1, RS_SHAPE_PULSE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1Saw)
{
  uint8_t regValue = 0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0x20).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceShape(RS_VOICE_1, RS_SHAPE_SAW));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1TriangleRegister0xFF)
{
  uint8_t regValue = 0xFF;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0x1F).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceShape(RS_VOICE_1, RS_SHAPE_TRIANGLE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1Triangle)
{
  uint8_t regValue = 0x0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0x10).
      andReturnValue(0);

  CHECK_EQUAL(0, SID_setVoiceShape(RS_VOICE_1, RS_SHAPE_TRIANGLE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1CannotWriteRegister)
{
  uint8_t regValue = 0x0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("SIDREGS_writeRegister").
      withParameter("reg", 0x04).
      withParameter("value", 0x10).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setVoiceShape(RS_VOICE_1, RS_SHAPE_TRIANGLE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1CannotReadRegister)
{
  uint8_t regValue = 0x0;

  mock().expectOneCall("SIDREGS_readRegister").
      withParameter("reg", 0x04).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SID_setVoiceShape(RS_VOICE_1, RS_SHAPE_TRIANGLE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1InvalidShape)
{
  CHECK_EQUAL(-1, SID_setVoiceShape(RS_VOICE_1, 1));
}

//------------------------------------------------------------------------------
TEST(SID, setShapeInvalidVoice)
{
  CHECK_EQUAL(-1, SID_setVoiceShape((RSVoice_t)12, RS_SHAPE_TRIANGLE));
}

//------------------------------------------------------------------------------
TEST(SID, initFails)
{
  int retVal = -1;

  mock().expectOneCall("SIDREGS_init").
      andReturnValue(retVal);

  CHECK_EQUAL(retVal, SID_init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, initSuccess)
{
  int retVal = 0;

  mock().expectOneCall("SIDREGS_init").
      andReturnValue(retVal);

  CHECK_EQUAL(retVal, SID_init());

  mock().checkExpectations();
}
