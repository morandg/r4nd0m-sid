/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>

#include "../src/SID_regs_private.h"
#include "../src/SID_regs.h"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

//------------------------------------------------------------------------------
TEST_GROUP(SIDREGS)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
    mock().clear();
  }

  void expectAllWORegistersSetToZero()
  {
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 0).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 1).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 2).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 3).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 4).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 5).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 6).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 7).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 8).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 9).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 10).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 11).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 12).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 13).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 14).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 15).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 16).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 17).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 18).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 19).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 20).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 21).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 22).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 23).
        withParameter("value", 0).
        andReturnValue(0);
    mock().expectOneCall("SIDIO_write").
        withParameter("address", 24).
        withParameter("value", 0).
        andReturnValue(0);
  }
};

//------------------------------------------------------------------------------
TEST(SIDREGS, writeRegisterRO)
{
  uint8_t value = 0xAB;
  uint8_t reg = 25;

  CHECK_EQUAL(-1, SIDREGS_writeRegister(reg, value));
}

//------------------------------------------------------------------------------
TEST(SIDREGS, writeRegisterWoLast)
{
  uint8_t value = 0xAB;
  uint8_t reg = 24;

  sid_regs_wo[reg] = 0;

  mock().expectOneCall("SIDIO_write").
      withParameter("address", reg).
      withParameter("value", value).
      andReturnValue(0);

  CHECK_EQUAL(0, SIDREGS_writeRegister(reg, value));

  mock().checkExpectations();

  CHECK_EQUAL(value, sid_regs_wo[reg]);
}

//------------------------------------------------------------------------------
TEST(SIDREGS, writeRegisterWoLastWriteFails)
{
  uint8_t value = 0xAB;
  uint8_t reg = 24;

  sid_regs_wo[reg] = 0;

  mock().expectOneCall("SIDIO_write").
      withParameter("address", reg).
      withParameter("value", value).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SIDREGS_writeRegister(reg, value));

  mock().checkExpectations();

  CHECK_EQUAL(0, sid_regs_wo[reg]);
}

//------------------------------------------------------------------------------
TEST(SIDREGS, writeRegisterOutOfRange)
{
  uint8_t value = 0x12;

  CHECK_EQUAL(-1, SIDREGS_writeRegister(29, value));
}

//------------------------------------------------------------------------------
TEST(SIDREGS, readRegisterRoLast)
{
  uint8_t readValue;
  uint8_t actualValue = 0xDE;
  uint8_t reg = 28;

  mock().expectOneCall("SIDIO_read").
      withParameter("address", reg).
      withOutputParameterReturning("value", &actualValue, sizeof(actualValue)).
      andReturnValue(0);

  CHECK_EQUAL(0, SIDREGS_readRegister(reg, &readValue));

  mock().checkExpectations();

  CHECK_EQUAL(actualValue, readValue);
}

//------------------------------------------------------------------------------
TEST(SIDREGS, readRegisterRoFirst)
{
  uint8_t readValue;
  uint8_t actualValue = 0xAB;
  uint8_t reg = 25;

  mock().expectOneCall("SIDIO_read").
      withParameter("address", reg).
      withOutputParameterReturning("value", &actualValue, sizeof(actualValue)).
      andReturnValue(0);

  CHECK_EQUAL(0, SIDREGS_readRegister(reg, &readValue));

  mock().checkExpectations();

  CHECK_EQUAL(actualValue, readValue);
}

//------------------------------------------------------------------------------
TEST(SIDREGS, readRegisterRoFirstReadRegFails)
{
  uint8_t readValue;
  uint8_t actualValue = 0xAB;
  uint8_t reg = 25;

  mock().expectOneCall("SIDIO_read").
      withParameter("address", reg).
      withOutputParameterReturning("value", &actualValue, sizeof(actualValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SIDREGS_readRegister(reg, &readValue));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDREGS, readRegisterWoLast)
{
  uint8_t readValue;
  uint8_t actualValue = 0xAB;
  uint8_t reg = 24;

  sid_regs_wo[reg] = actualValue;

  CHECK_EQUAL(0, SIDREGS_readRegister(reg, &readValue));
  CHECK_EQUAL(actualValue, readValue);
}

//------------------------------------------------------------------------------
TEST(SIDREGS, readRegisterOutOfRange)
{
  uint8_t value;

  CHECK_EQUAL(-1, SIDREGS_readRegister(29, &value));
}

//------------------------------------------------------------------------------
TEST(SIDREGS, initUpdateAllRegistersSucceedInternalCopyReseted)
{
  uint8_t expectedSidRegs[SID_REG_WO_LAST];

  std::memset(expectedSidRegs, 0, sizeof(expectedSidRegs));
  std::memset(sid_regs_wo, 0xA, sizeof(expectedSidRegs));

  mock().expectOneCall("SIDIO_init").
      andReturnValue(0);
  expectAllWORegistersSetToZero();

  CHECK_EQUAL(0, SIDREGS_init());

  mock().checkExpectations();

  MEMCMP_EQUAL(expectedSidRegs, sid_regs_wo, sizeof(expectedSidRegs));
}

//------------------------------------------------------------------------------
TEST(SIDREGS, initUpdateAllRegistersSucceed)
{
  mock().expectOneCall("SIDIO_init").
      andReturnValue(0);
  expectAllWORegistersSetToZero();

  CHECK_EQUAL(0, SIDREGS_init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDREGS, initUpdateSecondRegisterFails)
{
  mock().expectOneCall("SIDIO_init").
      andReturnValue(0);
  mock().expectOneCall("SIDIO_write").
      withParameter("address", 0).
      withParameter("value", 0).
      andReturnValue(0);
  mock().expectOneCall("SIDIO_write").
      withParameter("address", 1).
      withParameter("value", 0).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SIDREGS_init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDREGS, initUpdateFirstRegisterFails)
{
  mock().expectOneCall("SIDIO_init").
      andReturnValue(0);
  mock().expectOneCall("SIDIO_write").
      withParameter("address", 0).
      withParameter("value", 0).
      andReturnValue(-1);

  CHECK_EQUAL(-1, SIDREGS_init());

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDREGS, initInitSIDIOFails)
{
  int retVal =-1;

  mock().expectOneCall("SIDIO_init").
      andReturnValue(retVal);

  CHECK_EQUAL(retVal, SIDREGS_init());

  mock().checkExpectations();
}
