#!/bin/sh
#
# Generates MIDI notes to frequency lookup table, depending on the clock
# frequency connected to the SID chip
########################################################################
CLOCK_FREQ=1000000  # Hz

########################################################################
computeNoteFrequency() {
  BASE_FREQ=440
  FIRST_MIDI_NOTE_OFFSET=-57

  echo "${BASE_FREQ} * 2^((${1} + ${FIRST_MIDI_NOTE_OFFSET}) / 12)" | \
    octave | awk '{print $3}'
}

########################################################################
computeSidFrequency() {
  echo "round(${1} * (16777216 / ${CLOCK_FREQ}))" | \
    octave | awk '{print $3}'
}

########################################################################
# Main
# Header
echo "// AUTO GENERATED DO NOT MODIFY!!"
echo "#include \"midi_SID_freq_table.h\""
echo ""
echo "// Frequency table from C6 to B6"
echo "MidiFreqRef SID_freq_table[NOTES_PER_OCTAVES] = {"

for note in $(seq 72 83)
do
  note_freq=$(computeNoteFrequency ${note})
  sid_freq=$(computeSidFrequency ${note_freq})

  echo "    {"
  echo "        .midiNote = $(printf 0x%x ${note}),"
  echo "        .frequency = $(printf 0x%x ${sid_freq})"
  echo "    },"
done

echo "};"

