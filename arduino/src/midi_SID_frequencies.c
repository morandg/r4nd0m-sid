/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "midi_SID_freq_table.h"
#include "midi_SID_frequencies.h"

//------------------------------------------------------------------------------
static int findLowerOctaveFrequency(int offset, uint16_t* sidFreq)
{
  unsigned int octavesOffset = 0;

  while(offset < 0)
  {
    offset += NOTES_PER_OCTAVES;
    ++octavesOffset;
  }

  *sidFreq = SID_freq_table[offset].frequency;
  *sidFreq = *sidFreq >> octavesOffset;

  return 0;
}

//------------------------------------------------------------------------------
static int findHigherOctaveFrequency(int offset, uint16_t* sidFreq)
{
  unsigned int octavesOffset = 0;

  while(offset >= NOTES_PER_OCTAVES)
  {
    offset -= NOTES_PER_OCTAVES;
    ++octavesOffset;
  }

  *sidFreq = SID_freq_table[offset].frequency;

  // Check overflow
  for(unsigned int i = 0 ; i < octavesOffset ; ++i)
  {
    if(*sidFreq & 0x8000)
    {
      *sidFreq = 0;
      return -1;
    }

    *sidFreq = *sidFreq << 1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int SIDFREQ_midiToSidFreq(uint8_t midi, uint16_t* sidFreq)
{
  int freqOffset = midi - SID_freq_table[0].midiNote;

  if(freqOffset < 0)
    return findLowerOctaveFrequency(freqOffset, sidFreq);
  else
    return findHigherOctaveFrequency(freqOffset, sidFreq);
}
