/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>

#include "SID_IO.h"

//------------------------------------------------------------------------------
int SIDIO_init()
{
  // TODO PIN muxing

  return 0;
}

//------------------------------------------------------------------------------
int SIDIO_read(uint8_t address, uint8_t* value)
{
  // TODO
  (void)address;
  (void)value;

  return 0;
}

//------------------------------------------------------------------------------
int SIDIO_write(uint8_t address, uint8_t value)
{
  // TODO
  (void)address;
  (void)value;

  return 0;
}
