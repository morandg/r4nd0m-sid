/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_SID_H_
#define _RANDOM_SID_SID_H_

#include "SIDDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

int SID_init();
int SID_setVoiceShape(enum RSVoice_t voice, uint8_t shape);
int SID_setVoiceFrequency(enum RSVoice_t voice, RSSIDFrequency frequency);
int SID_startVoice(enum RSVoice_t voice);
int SID_releaseVoice(enum RSVoice_t voice);
int SID_setVolume(uint8_t volume);
int SID_setAttackDecay(enum RSVoice_t voice, uint8_t attackDecay);
int SID_setSustainRelease(enum RSVoice_t voice, uint8_t sustainRelease);
int SID_setPulseWidth(enum RSVoice_t voice, uint16_t pulseWidth);

// TODO
// Filter

#ifdef __cplusplus
}   // extern "C"
#endif

#endif  // _RANDOM_SID_SID_H_
