/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_MIDI_SID_FREQ_TABLE_H_
#define _RANDOM_SID_MIDI_SID_FREQ_TABLE_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define NOTES_PER_OCTAVES   12

typedef struct MidiFreqRef_t {
  uint8_t midiNote;
  uint16_t frequency;
} MidiFreqRef;

extern MidiFreqRef SID_freq_table[NOTES_PER_OCTAVES];

#ifdef __cplusplus
}   // extern "C"
#endif

#endif  // _RANDOM_SID_MIDI_SID_FREQ_TABLE_H_
