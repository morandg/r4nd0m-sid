/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>

#include "SID_IO.h"
#include "SID_regs_private.h"
#include "SID_regs.h"

uint8_t sid_regs_wo[SID_REG_WO_LAST];

//------------------------------------------------------------------------------
int SIDREGS_init()
{
  if(SIDIO_init())
    return -1;

  memset(sid_regs_wo, 0, sizeof(sid_regs_wo));
  for(uint8_t reg = 0 ; reg <= SID_REG_WO_LAST ; ++reg)
  {
    if(SIDIO_write(reg, sid_regs_wo[reg]))
      return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int SIDREGS_readRegister(uint8_t reg, uint8_t* value)
{
  if(reg > SID_REG_LAST)
    return -1;

  if(reg <= SID_REG_WO_LAST)
  {
    *value = sid_regs_wo[reg];
    return 0;
  }

  return SIDIO_read(reg, value);
}

//------------------------------------------------------------------------------
int SIDREGS_writeRegister(uint8_t reg, uint8_t value)
{
  if(reg > SID_REG_WO_LAST)
    return -1;

  if(SIDIO_write(reg, value))
    return -1;
  sid_regs_wo[reg] = value;

  return 0;
}
