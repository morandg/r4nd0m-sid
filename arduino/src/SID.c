/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SID.h"
#include "SID_regs.h"

#define SID_REG_VOICE_1_FREQ_LOW  (uint8_t)0x00
#define SID_REG_VOICE_1_FREQ_HIGH (uint8_t)0x01
#define SID_REG_VOICE_1_PW_LOW    (uint8_t)0x02
#define SID_REG_VOICE_1_PW_HIGH   (uint8_t)0x03
#define SID_REG_VOICE_1_CTRL      (uint8_t)0x04
#define SID_REG_VOICE_1_ATK_DCY   (uint8_t)0x05
#define SID_REG_VOICE_1_STN_RIS   (uint8_t)0x06
#define SID_REG_VOICE_2_FREQ_LOW  (uint8_t)0x07
#define SID_REG_VOICE_2_FREQ_HIGH (uint8_t)0x08
#define SID_REG_VOICE_2_PW_LOW    (uint8_t)0x09
#define SID_REG_VOICE_2_PW_HIGH   (uint8_t)0x0A
#define SID_REG_VOICE_2_CTRL      (uint8_t)0x0B
#define SID_REG_VOICE_2_ATK_DCY   (uint8_t)0x0C
#define SID_REG_VOICE_2_STN_RIS   (uint8_t)0x0D
#define SID_REG_VOICE_3_FREQ_LOW  (uint8_t)0x0E
#define SID_REG_VOICE_3_FREQ_HIGH (uint8_t)0x0F
#define SID_REG_VOICE_3_PW_LOW    (uint8_t)0x10
#define SID_REG_VOICE_3_PW_HIGH   (uint8_t)0x11
#define SID_REG_VOICE_3_CTRL      (uint8_t)0x12
#define SID_REG_VOICE_3_ATK_DCY   (uint8_t)0x13
#define SID_REG_VOICE_3_STN_RIS   (uint8_t)0x14
#define SID_REG_MODE_VOL          (uint8_t)0x18

#define SID_BIT_GATE              (uint8_t)0x01

//------------------------------------------------------------------------------
static int voiceToCtrlRegister(enum RSVoice_t voice, uint8_t* ctrlReg)
{
  switch(voice)
  {
    case RS_VOICE_1:
      *ctrlReg = SID_REG_VOICE_1_CTRL;
      break;
    case RS_VOICE_2:
      *ctrlReg = SID_REG_VOICE_2_CTRL;
      break;
    case RS_VOICE_3:
      *ctrlReg = SID_REG_VOICE_3_CTRL;
      break;
    default:
      return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
int SID_init()
{
  return SIDREGS_init();
}

//------------------------------------------------------------------------------
int SID_setVoiceShape(enum RSVoice_t voice, uint8_t shape)
{
  uint8_t regValue;
  uint8_t ctrlReg;

  if((shape & 0x0f) ||
      voiceToCtrlRegister(voice, &ctrlReg))
    return -1;

  if(SIDREGS_readRegister(ctrlReg, &regValue))
    return -1;

  regValue &= 0x0F;
  regValue |= shape;

  return SIDREGS_writeRegister(ctrlReg, regValue);
}

//------------------------------------------------------------------------------
int SID_setVoiceFrequency(enum RSVoice_t voice,  RSSIDFrequency frequency)
{
  uint8_t freqRegLow;
  uint8_t freqRegHigh;

  switch(voice)
  {
    case RS_VOICE_1:
      freqRegLow = SID_REG_VOICE_1_FREQ_LOW;
      freqRegHigh = SID_REG_VOICE_1_FREQ_HIGH;
      break;
    case RS_VOICE_2:
      freqRegLow = SID_REG_VOICE_2_FREQ_LOW;
      freqRegHigh = SID_REG_VOICE_2_FREQ_HIGH;
      break;
    case RS_VOICE_3:
      freqRegLow = SID_REG_VOICE_3_FREQ_LOW;
      freqRegHigh = SID_REG_VOICE_3_FREQ_HIGH;
      break;
    default:
      return -1;
  }

  if(SIDREGS_writeRegister(freqRegLow, frequency) ||
      SIDREGS_writeRegister(freqRegHigh, frequency >> 8))
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
int SID_startVoice(enum RSVoice_t voice)
{
  uint8_t ctrlReg;
  uint8_t regValue;

  if(voiceToCtrlRegister(voice, &ctrlReg) ||
      SIDREGS_readRegister(ctrlReg, &regValue))
    return -1;

  regValue |= SID_BIT_GATE;

  return SIDREGS_writeRegister(ctrlReg, regValue);
}

//------------------------------------------------------------------------------
int SID_releaseVoice(enum RSVoice_t voice)
{
  uint8_t ctrlReg;
  uint8_t regValue;

  if(voiceToCtrlRegister(voice, &ctrlReg) ||
      SIDREGS_readRegister(ctrlReg, &regValue))
    return -1;

  regValue &= ~SID_BIT_GATE;

  return SIDREGS_writeRegister(ctrlReg, regValue);
}

//------------------------------------------------------------------------------
int SID_setVolume(uint8_t volume)
{
  uint8_t regValue;

  if(volume & 0xF0)
    return -1;

  if(SIDREGS_readRegister(SID_REG_MODE_VOL, &regValue))
    return -1;

  regValue = (regValue & 0xF0) | volume;
  return SIDREGS_writeRegister(SID_REG_MODE_VOL, regValue);

  return 0;
}

//------------------------------------------------------------------------------
int SID_setAttackDecay(enum RSVoice_t voice, uint8_t attackDecay)
{
  uint8_t reg;

  switch(voice)
  {
    case RS_VOICE_1:
      reg = SID_REG_VOICE_1_ATK_DCY;
      break;
    case RS_VOICE_2:
      reg = SID_REG_VOICE_2_ATK_DCY;
      break;
    case RS_VOICE_3:
      reg = SID_REG_VOICE_3_ATK_DCY;
      break;
    default:
      return -1;
  }

  return SIDREGS_writeRegister(reg, attackDecay);
}

//------------------------------------------------------------------------------
int SID_setSustainRelease(enum RSVoice_t voice, uint8_t sustainRelease)
{
  uint8_t reg;

  switch(voice)
  {
    case RS_VOICE_1:
      reg = SID_REG_VOICE_1_STN_RIS;
      break;
    case RS_VOICE_2:
      reg = SID_REG_VOICE_2_STN_RIS;
      break;
    case RS_VOICE_3:
      reg = SID_REG_VOICE_3_STN_RIS;
      break;
    default:
      return -1;
  }

  return SIDREGS_writeRegister(reg, sustainRelease);
}

//------------------------------------------------------------------------------
int SID_setPulseWidth(enum RSVoice_t voice, uint16_t pulseWidth)
{
  uint8_t regPwLow;
  uint8_t regPwHigh;

  if(pulseWidth & 0xF000)
    return -1;

  switch(voice)
  {
    case RS_VOICE_1:
      regPwLow = SID_REG_VOICE_1_PW_LOW;
      regPwHigh = SID_REG_VOICE_1_PW_HIGH;
      break;
    case RS_VOICE_2:
      regPwLow = SID_REG_VOICE_2_PW_LOW;
      regPwHigh = SID_REG_VOICE_2_PW_HIGH;
      break;
    case RS_VOICE_3:
      regPwLow = SID_REG_VOICE_3_PW_LOW;
      regPwHigh = SID_REG_VOICE_3_PW_HIGH;
      break;
    default:
      return -1;
  }

  if(SIDREGS_writeRegister(regPwLow, pulseWidth) ||
      SIDREGS_writeRegister(regPwHigh, pulseWidth >> 8))
    return -1;

  return 0;
}
