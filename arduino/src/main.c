/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <util/delay.h>

#include "SID.h"
#include "portamento.h"
#include "midi_parser.h"

int main(void)
{
  SID_init();
  PORTA_init();
  MIDIPrsr_init();

  // For testing
  SID_setVoiceShape(RS_VOICE_1, RS_SHAPE_TRIANGLE | RS_SHAPE_SAW);
  SID_setSustainRelease(RS_VOICE_1, 0xF0);

  SID_setVoiceShape(RS_VOICE_2, RS_SHAPE_PULSE);
  SID_setPulseWidth(RS_VOICE_2, 0x800);
  SID_setSustainRelease(RS_VOICE_2, 0xF0);

  SID_setVoiceShape(RS_VOICE_3, RS_SHAPE_NOISE);
  SID_setSustainRelease(RS_VOICE_3, 0xF0);

  SID_setVolume(0xF);

  while(1)
  {
    // TODO

    // For testing
    uint8_t midiInput = 0x00;

    // Read serial for commands

    // Apply commands on SID
    MIDIPrsr_onInput(midiInput);
  }
}
