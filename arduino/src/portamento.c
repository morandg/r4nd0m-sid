/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>

#include "midi_SID_frequencies.h"
#include "portamento_private.h"
#include "portamento.h"

PortaStatus portaStatus;

//------------------------------------------------------------------------------
int PORTA_init()
{
  memset(&portaStatus, 0, sizeof(portaStatus));

  return 0;
}

//------------------------------------------------------------------------------
int PORTA_startNote(enum RSVoice_t voice, uint8_t note)
{
  uint16_t sidFreq;

  if(SIDFREQ_midiToSidFreq(note, &sidFreq) ||
      SID_setVoiceFrequency(voice, sidFreq) ||
      SID_startVoice(voice))
    return -1;

  // TODO Stack notes to play back the last pressed
  portaStatus.voiceStatus[voice].currentNote = note;

  return 0;
}

//------------------------------------------------------------------------------
int PORTA_releaseNote(enum RSVoice_t voice, uint8_t note)
{
  int retCode = 0;

  if(portaStatus.voiceStatus[voice].currentNote == note)
    retCode = SID_releaseVoice(voice);

  // TODO Remove note from stack and set the frequency of the one at the top
  portaStatus.voiceStatus[voice].currentNote = 0;

  return retCode;
}
