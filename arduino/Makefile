include ../common.mk

# Firmware sources
SOURCES = \
  src/portamento.c \
  src/midi_parser.c \
  src/midi_SID_frequencies.c \
  src/midi_SID_freq_table.c \
  src/main.c \
  src/SID_regs.c \
  src/SID_IO.c \
  src/SID.c

APP_NAME          = r4nd0m-sid-arduino
BUILD_DIR         ?= ../build/arduino
OBJS              = $(patsubst src/%.c,$(BUILD_DIR)/%.o,$(SOURCES))
DEP               = $(OBJS:.o=.dep)
CFLAGS            += -I../common
CXXFLAGS          += -I../common



# TODO rebuild the tests

# All depedencies
DEP               = $(OBJS:.o=.d)

##############################################################################################
# Default
##############################################################################################
DEFAULT_TARGET   = firmware

ifeq ($(HAS_CPPUTEST),1)
  DEFAULT_TARGET += tests
endif

all: $(DEFAULT_TARGET)
.PHONY:all

-include $(DEP)

##############################################################################################
# Firmware for arduino
##############################################################################################
firmware: $(BUILD_DIR)/$(APP_NAME).hex
.PHONY: firmware

src/midi_SID_freq_table.c: scripts/gen-freq-lookup-table.sh src/midi_SID_freq_table.h
	@printf $(PRINTF_CMD_FMT) "[ GEN ]"  "$@"
	$(SILENCE)$< > $@

$(BUILD_DIR)/$(APP_NAME).hex: $(BUILD_DIR)/$(APP_NAME).elf
	@printf $(PRINTF_CMD_FMT) "[ $(AVR_OBJCPY) ]"  "$@"
	$(SILENCE)$(AVR_OBJCPY) -j .text -j .data -O ihex $< $@
	$(SILENCE)$(AVR_OBJCPY) -j .eeprom --set-section-flags=.eeprom="alloc,load" --change-section-lma .eeprom=0 -O ihex $< $(BUILD_DIR)/eeprom.hex

$(BUILD_DIR)/$(APP_NAME).elf: $(OBJS)
	@printf $(PRINTF_CMD_FMT) "[ $(AVR_LD) ]"  "$@"
	$(SILENCE)$(AVR_LD) $(AVR_LDFLAGS) $(OBJS) -o $@

$(BUILD_DIR)/%.o: src/%.c
ifneq ($(HAS_AVR_CC),1)
	$(error AVR compile $(AVR_CC) not found, cannot compile arduino firmware!)
endif
	@printf $(PRINTF_CMD_FMT) "[ $(AVR_CC) ]"  "$@ ..."
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(AVR_CC) $(AVR_CFLAGS) -c -MMD $< -o $@

##############################################################################################
# Cleanup
##############################################################################################
clean:
	@printf $(PRINTF_CMD_FMT) "[ CLEAN ]"  "$(APP_NAME)"
	$(SILENCE)rm -rf $(BUILD_DIR) src/midi_SID_freq_table.cm
.PHONY: clean