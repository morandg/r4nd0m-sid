/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/spinctrl.h>

#include "VoiceShapePanel.hpp"

namespace r4nd0mSID {

//------------------------------------------------------------------------------
VoiceShapePanel::VoiceShapePanel(
    wxWindow* parent, ISidDevice& sidDevice, RSVoice_t voice):
  wxPanel(parent),
  m_sidDevice(sidDevice),
  m_voice(voice)
{
}

//------------------------------------------------------------------------------
VoiceShapePanel::~VoiceShapePanel()
{
}

//------------------------------------------------------------------------------
int VoiceShapePanel::init()
{
  wxSizer* sizer;

  sizer = new wxGridSizer(3, 2, 3, 3);

  m_noiseCheckBox = new wxCheckBox(this, wxID_ANY, wxString("Noise"));
  sizer->Add(m_noiseCheckBox, 0, wxEXPAND);
  m_pulseCheckBox = new wxCheckBox(this, wxID_ANY, wxString("Pulse"));
  sizer->Add(m_pulseCheckBox, 0, wxEXPAND);
  m_sawCheckBox = new wxCheckBox(this, wxID_ANY, wxString("Saw"));
  sizer->Add(m_sawCheckBox, 0, wxEXPAND);
  m_triangleCheckBox = new wxCheckBox(this, wxID_ANY, wxString("Triangle"));
  sizer->Add(m_triangleCheckBox, 0, wxEXPAND);
  Bind(
      wxEVT_CHECKBOX,
      &VoiceShapePanel::OnVoicheShapeChanged,
      this);

  sizer->Add(
      new wxStaticText(this, wxID_ANY, wxString("Pulse width:")), 0, 0, 0);
  sizer->Add(
      new wxSpinCtrl(
          this,
          wxID_ANY,
          wxEmptyString,
          wxDefaultPosition,
          wxDefaultSize,
          wxSP_ARROW_KEYS,
          0,
          RS_MAX_SID_PW,
          0
      ), 0, 0, 0);
  Bind(
      wxEVT_SPINCTRL,
      &VoiceShapePanel::onPWChanged,
      this);

  SetSizer(sizer);

  return 0;
}

//------------------------------------------------------------------------------
void VoiceShapePanel::OnVoicheShapeChanged(wxCommandEvent& event)
{
  uint8_t shape = 0;

  if(m_noiseCheckBox->IsChecked())
    shape |= RS_SHAPE_NOISE;
  if(m_pulseCheckBox->IsChecked())
    shape |= RS_SHAPE_PULSE;
  if(m_sawCheckBox->IsChecked())
    shape |= RS_SHAPE_SAW;
  if(m_triangleCheckBox->IsChecked())
    shape |= RS_SHAPE_TRIANGLE;

  m_sidDevice.setVoiceShape(m_voice, shape);
}

//------------------------------------------------------------------------------
void VoiceShapePanel::onPWChanged(wxSpinEvent& event)
{
  m_sidDevice.setPulseWidth(m_voice, event.GetPosition());
}

}       // namespace
