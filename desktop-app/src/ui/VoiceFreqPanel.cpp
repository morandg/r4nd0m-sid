/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/checkbox.h>
#include <wx/spinctrl.h>

#include "VoiceFreqPanel.hpp"

namespace r4nd0mSID {

enum EVENT_CHECKBOX {
  EVENT_GATE  = 500,
  EVENT_RING  = 501,
  EVENT_SYNC  = 502
};

//------------------------------------------------------------------------------
VoiceFreqPanel::VoiceFreqPanel(
    wxWindow* parent, ISidDevice& sidDevice, RSVoice_t voice):
  wxPanel(parent),
  m_sidDevice(sidDevice),
  m_voice(voice),
  m_lastValidFreq("")
{
}

//------------------------------------------------------------------------------
VoiceFreqPanel::~VoiceFreqPanel()
{
}

//------------------------------------------------------------------------------
int VoiceFreqPanel::init()
{
  wxSizer* mainSizer;
  wxSizer* hbox1;
  wxSpinCtrl* voiceFreq;

  mainSizer = new wxBoxSizer(wxVERTICAL);
  hbox1 = new wxBoxSizer(wxHORIZONTAL);

  // Frequency label + input
  hbox1->Add(
      new wxStaticText(this, wxID_ANY, wxString("Frequency:")),
      0, 0, 0);

  voiceFreq = new wxSpinCtrl(
      this,
      wxID_ANY,
      wxEmptyString,
      wxDefaultPosition,
      wxDefaultSize,
      wxSP_ARROW_KEYS,
      0,
      RS_MAX_SID_FREQUENCY,
      0);
  Bind(
      wxEVT_SPINCTRL,
      &VoiceFreqPanel::onFrequencyChanged,
      this);
  hbox1->Add(voiceFreq, 0, 0, 0);
  mainSizer->Add(hbox1);

  // Gate checkbox
  mainSizer->Add(
     new wxCheckBox(this, EVENT_GATE, wxString("GATE")),
     0, 0, 0);
  Bind(
      wxEVT_CHECKBOX,
      &VoiceFreqPanel::onGateChanged,
      this,
      EVENT_GATE);

  // RING checkbox
  mainSizer->Add(
     new wxCheckBox(this, EVENT_RING, wxString("RING")),
     0, 0, 0);
  Bind(
      wxEVT_CHECKBOX,
      &VoiceFreqPanel::onRingChanged,
      this,
      EVENT_RING);

  // Sync checkbox
  mainSizer->Add(
     new wxCheckBox(this, EVENT_SYNC, wxString("SYNC")),
     0, 0, 0);
  Bind(
      wxEVT_CHECKBOX,
      &VoiceFreqPanel::onSyncChanged,
      this,
      EVENT_SYNC);

  SetSizer(mainSizer);

  return 0;
}

//------------------------------------------------------------------------------
void VoiceFreqPanel::onFrequencyChanged(wxSpinEvent& event)
{
  m_sidDevice.setVoiceFrequency(m_voice, event.GetPosition());
}

//------------------------------------------------------------------------------
void VoiceFreqPanel::onGateChanged(wxCommandEvent& event)
{
  if(event.IsChecked())
    m_sidDevice.startVoice(m_voice);
  else
    m_sidDevice.releaseVoice(m_voice);
}

//------------------------------------------------------------------------------
void VoiceFreqPanel::onRingChanged(wxCommandEvent& event)
{
  m_sidDevice.enableRing(m_voice, event.IsChecked());
}

//------------------------------------------------------------------------------
void VoiceFreqPanel::onSyncChanged(wxCommandEvent& event)
{
  m_sidDevice.enableSync(m_voice, event.IsChecked());
}

}       // namespace
