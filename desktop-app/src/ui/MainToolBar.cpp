/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>

#include "MainToolBar.hpp"
#include "Resources.hpp"

namespace r4nd0mSID {

//------------------------------------------------------------------------------
MainToolBar::MainToolBar(wxWindow* parent, ISidDevice& sidDevice):
    wxToolBar(parent, wxID_ANY),
    m_sidDevice(sidDevice),
    m_lastVolumeValue(15),
    m_volumeSlider(nullptr),
    m_volumeButton(nullptr)
{
}

//------------------------------------------------------------------------------
MainToolBar::~MainToolBar()
{
}

//------------------------------------------------------------------------------
int MainToolBar::init()
{
  if(createVolumeSlider())
    return -1;
  if(createVolumeButton())
    return -1;

  Realize();

  return 0;
}

//------------------------------------------------------------------------------
void MainToolBar::OnVolumeChanged(wxCommandEvent& event)
{
  updateVolume(m_volumeSlider->GetValue());
}

//------------------------------------------------------------------------------
void MainToolBar::OnVolumeClicked(wxCommandEvent& event)
{
  int currentVolume = m_volumeSlider->GetValue();

  if(currentVolume > 0)
    updateVolume(0);
  else
    updateVolume(m_lastVolumeValue);
}

//------------------------------------------------------------------------------
void MainToolBar::updateVolume(int volume)
{
  m_sidDevice.setVolume(static_cast<uint8_t>(volume));

  if(volume > 0)
    m_lastVolumeValue = volume;

  m_volumeSlider->SetValue(volume);
  updateVolumeButton();
}

//------------------------------------------------------------------------------
int MainToolBar::createVolumeSlider()
{
  m_volumeSlider = new wxSlider(
      this,
      wxID_ANY,
      0,                  // value
      0,                  // min
      15,                 // max
      wxDefaultPosition,
      wxSize(140, -1),
      wxSL_HORIZONTAL);
  m_volumeSlider->SetToolTip(wxString("Volume"));

  AddControl(m_volumeSlider, wxT("Volume"));

  Bind(
      wxEVT_COMMAND_SLIDER_UPDATED,
      &MainToolBar::OnVolumeChanged,
      this);

  return 0;
}

//------------------------------------------------------------------------------
int MainToolBar::createVolumeButton()
{
  m_volumeButton = new wxBitmapButton(
      this,
      wxID_ANY,
      getVolumeIcon());

  AddControl(m_volumeButton, wxT("Volume"));

  Bind(
      wxEVT_BUTTON,
      &MainToolBar::OnVolumeClicked,
      this);

  return 0;
}

//------------------------------------------------------------------------------
void MainToolBar::updateVolumeButton()
{
  m_volumeButton->SetBitmapLabel(getVolumeIcon());
}

//------------------------------------------------------------------------------
wxBitmap& MainToolBar::getVolumeIcon()
{
  if(m_volumeSlider->GetValue() == 0)
    return Resources::getInstance().getIcon(Resources::VOLUME_MUTE);
  else if(m_volumeSlider->GetValue() < 8)
    return Resources::getInstance().getIcon(Resources::VOLUME_LOW);
  else if(m_volumeSlider->GetValue() < 15)
    return Resources::getInstance().getIcon(Resources::VOLUME_MED);
  else
    return Resources::getInstance().getIcon(Resources::VOLUME_HIGH);
}

}       // namespace
