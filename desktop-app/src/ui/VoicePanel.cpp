/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/stattext.h>

#include "VoiceShapePanel.hpp"
#include "VoiceFreqPanel.hpp"
#include "VoiceADSRPanel.hpp"
#include "VoicePanel.hpp"

namespace r4nd0mSID {

//------------------------------------------------------------------------------
VoicePanel::VoicePanel(
    wxWindow* parent, ISidDevice& sidDevice, RSVoice_t voice):
  wxPanel(parent),
  m_sidDevice(sidDevice),
  m_voice(voice)
{
}

//------------------------------------------------------------------------------
VoicePanel::~VoicePanel()
{
}

//------------------------------------------------------------------------------
int VoicePanel::init()
{
  wxString labelString;
  VoiceFreqPanel* voiceFreqPanel;
  VoiceShapePanel* voiceShapePanel;
  VoiceADSRPanel* voiceADSRPanel;
  wxSizer* mainSizer;
  wxSizer* voiceElSizer;

  mainSizer = new wxBoxSizer(wxVERTICAL);
  voiceElSizer = new wxBoxSizer(wxHORIZONTAL);

  labelString << "Voice " << static_cast<unsigned int>(m_voice) + 1;
  mainSizer->Add(new wxStaticText(this, wxID_ANY, labelString), 0, 0, 0);

  voiceFreqPanel = new VoiceFreqPanel(this, m_sidDevice, m_voice);
  if(voiceFreqPanel->init())
    return -1;
  voiceElSizer->Add(voiceFreqPanel, 1, 0, 0);

  voiceShapePanel = new VoiceShapePanel(this, m_sidDevice, m_voice);
  if(voiceShapePanel->init())
    return -1;
  voiceElSizer->Add(voiceShapePanel, 1, 0, 0);

  voiceADSRPanel = new VoiceADSRPanel(this, m_sidDevice, m_voice);
  if(voiceADSRPanel->init())
    return -1;
  voiceElSizer->Add(voiceADSRPanel, 1, 0, 0);

  mainSizer->Add(voiceElSizer, 0, 0, 0);
  SetSizer(mainSizer);

  return 0;
}

}       // namespace
