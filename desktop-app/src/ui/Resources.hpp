/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_RESSOURCES_HPP_
#define _RANDOM_SID_RESSOURCES_HPP_

#include <unordered_map>
#include <list>

#include <wx/bitmap.h>

#include "../sid/ISidDevice.hpp"

namespace r4nd0mSID {

class Resources
{
public:
  enum Icon {
    VOLUME_MUTE,
    VOLUME_LOW,
    VOLUME_MED,
    VOLUME_HIGH,
  };

  Resources& operator=(const Resources& rhs) = delete;
  ~Resources();

  static Resources& getInstance();

  wxBitmap& getIcon(Icon icon);

private:
  std::unordered_map<std::string, wxBitmap> m_icons;
  wxBitmap m_bitmapEmpty;
  wxString m_iconsPath;

  Resources();
  wxBitmap createIconBitmap(const wxString& iconPath);
  std::list<wxString> getIconsSearchPaths();
};

}       // namespace
#endif  // _RANDOM_SID_RESSOURCES_HPP_
