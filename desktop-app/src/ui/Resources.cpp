/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <wx/stdpaths.h>
#include <wx/filefn.h>

#include "Resources.hpp"

static const wxString PATH_VOL_MUTED  = "audio-volume-muted.png";
static const wxString PATH_VOL_LOW    = "audio-volume-low.png";
static const wxString PATH_VOL_MED    = "audio-volume-medium.png";
static const wxString PATH_VOL_HIGH   = "audio-volume-high.png";

namespace r4nd0mSID {

//------------------------------------------------------------------------------
Resources::~Resources()
{
}

//------------------------------------------------------------------------------
Resources::Resources()
{
}

//------------------------------------------------------------------------------
Resources& Resources::getInstance()
{
  static Resources ressources;

  return ressources;
}

//------------------------------------------------------------------------------
wxBitmap& Resources::getIcon(Icon icon)
{
  wxString iconPath;

  switch(icon)
  {
  case VOLUME_MUTE:
    iconPath = PATH_VOL_MUTED;
    break;
  case VOLUME_LOW:
    iconPath = PATH_VOL_LOW;
    break;
  case VOLUME_MED:
    iconPath = PATH_VOL_MED;
    break;
  case VOLUME_HIGH:
    iconPath = PATH_VOL_HIGH;
    break;
  }

  if(m_icons.find(iconPath.ToStdString()) == m_icons.end())
    m_icons[iconPath.ToStdString()] = createIconBitmap(iconPath);

  return m_icons[iconPath.ToStdString()];
}

//------------------------------------------------------------------------------
wxBitmap Resources::createIconBitmap(const wxString& iconPath)
{
  for(auto& prefix: getIconsSearchPaths())
  {
    wxString fullPath = prefix.Append(iconPath);

    if(wxFileExists(fullPath))
      return wxBitmap(fullPath, wxBITMAP_TYPE_PNG);
  }

  std::cerr << "Icon not found: " << iconPath << std::endl;

  return wxBitmap();
}

//------------------------------------------------------------------------------
std::list<wxString> Resources::getIconsSearchPaths()
{
  std::list<wxString> searchPaths;

  searchPaths.push_back(
      wxPathOnly(wxStandardPaths::Get().GetExecutablePath()).
      Append("/icons/"));
  searchPaths.push_back(
      wxPathOnly(wxStandardPaths::Get().GetExecutablePath()).
      Append("/../icons/"));

  return searchPaths;
}

}       // namespace
