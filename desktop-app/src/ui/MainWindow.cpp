/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <wx/sizer.h>
#include <wx/statline.h>

#include "MainMenu.hpp"
#include "MainToolBar.hpp"
#include "VoicePanel.hpp"
#include "MainWindow.hpp"

namespace r4nd0mSID {

//------------------------------------------------------------------------------
MainWindow::MainWindow(const wxString& title):
    wxFrame(nullptr, wxID_ANY, title)
{
}

//------------------------------------------------------------------------------
MainWindow::~MainWindow()
{
}

//------------------------------------------------------------------------------
int MainWindow::init(ISidDevice& sidDevice)
{
  MainMenu* mainMenu;
  MainToolBar* mainToolBar;
  VoicePanel* voice1Panel;
  VoicePanel* voice2Panel;
  VoicePanel* voice3Panel;
  wxSizer* panelsSizer;

  wxImage::AddHandler(new wxPNGHandler());

  mainMenu = new MainMenu(this);
  if(mainMenu->init())
    return 1;
  SetMenuBar(mainMenu);

  mainToolBar = new MainToolBar(this, sidDevice);
  if(mainToolBar->init())
    return -1;
  SetToolBar(mainToolBar);

  // XXX Create a main panel ...
  panelsSizer = new wxBoxSizer(wxVERTICAL);
  voice1Panel = new VoicePanel(this, sidDevice, RS_VOICE_1);
  if(voice1Panel->init())
    return -1;
  panelsSizer->Add(voice1Panel, 0, 0, 0);

  panelsSizer->Add(
      new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxSize(100, 10)),
      0, 0, 0);

  voice2Panel = new VoicePanel(this, sidDevice, RS_VOICE_2);
  if(voice2Panel->init())
    return -1;
  panelsSizer->Add(voice2Panel, 0, 0, 0);

  panelsSizer->Add(
      new wxStaticLine(this, wxID_ANY, wxDefaultPosition, wxSize(100, 10)),
      0, 0, 0);

  voice3Panel = new VoicePanel(this, sidDevice, RS_VOICE_3);
  if(voice3Panel->init())
    return -1;
  panelsSizer->Add(voice3Panel, 0, 0, 0);

  SetSizer(panelsSizer);

  Fit();
  Center();

  return 0;
}

}       // namespace
