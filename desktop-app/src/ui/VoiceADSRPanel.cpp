/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "VoiceADSRPanel.hpp"

#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/spinctrl.h>

namespace r4nd0mSID {

enum ADSR_EVENT {
  EVENT_ATTACK  = 500,
  EVENT_DECAY   = 501,
  EVENT_SUSTAIN = 502,
  EVENT_RELEASE = 503
};

//------------------------------------------------------------------------------
VoiceADSRPanel::VoiceADSRPanel(
    wxWindow* parent, ISidDevice& sidDevice, RSVoice_t voice):
  wxPanel(parent),
  m_sidDevice(sidDevice),
  m_voice(voice)
{
}

//------------------------------------------------------------------------------
VoiceADSRPanel::~VoiceADSRPanel()
{
}

//------------------------------------------------------------------------------
int VoiceADSRPanel::init()
{
  wxSizer* sizer;

  sizer = new wxGridSizer(2, 4, 3, 3);

  sizer->Add(new wxStaticText(this, wxID_ANY, wxString("Attack:")), 0, 0, 0);
  sizer->Add(
      new wxSpinCtrl(
          this,
          EVENT_ATTACK,
          wxEmptyString,
          wxDefaultPosition,
          wxDefaultSize,
          wxSP_ARROW_KEYS,
          0,
          RS_MAX_SID_ADSR,
          0
      ), 0, 0, 0);
  Bind(
      wxEVT_SPINCTRL,
      &VoiceADSRPanel::onAttackChanged,
      this,
      EVENT_ATTACK);

  sizer->Add(new wxStaticText(this, wxID_ANY, wxString("Decay:")), 0, 0, 0);
  sizer->Add(
      new wxSpinCtrl(
          this,
          EVENT_DECAY,
          wxEmptyString,
          wxDefaultPosition,
          wxDefaultSize,
          wxSP_ARROW_KEYS,
          0,
          RS_MAX_SID_ADSR,
          0
      ), 0, 0, 0);
  Bind(
      wxEVT_SPINCTRL,
      &VoiceADSRPanel::onDecayChanged,
      this,
      EVENT_DECAY);

  sizer->Add(new wxStaticText(this, wxID_ANY, wxString("Sustain:")), 0, 0, 0);
  sizer->Add(
      new wxSpinCtrl(
          this,
          EVENT_SUSTAIN,
          wxEmptyString,
          wxDefaultPosition,
          wxDefaultSize,
          wxSP_ARROW_KEYS,
          0,
          RS_MAX_SID_ADSR,
          0
      ), 0, 0, 0);
  Bind(
      wxEVT_SPINCTRL,
      &VoiceADSRPanel::onSustainChanged,
      this,
      EVENT_SUSTAIN);

  sizer->Add(new wxStaticText(this, wxID_ANY, wxString("Release:")), 0, 0, 0);
  sizer->Add(
      new wxSpinCtrl(
          this,
          EVENT_RELEASE,
          wxEmptyString,
          wxDefaultPosition,
          wxDefaultSize,
          wxSP_ARROW_KEYS,
          0,
          RS_MAX_SID_ADSR,
          0
      ), 0, 0, 0);
  Bind(
      wxEVT_SPINCTRL,
      &VoiceADSRPanel::onReleaseChanged,
      this,
      EVENT_RELEASE);


  SetSizer(sizer);

  return 0;
}

//------------------------------------------------------------------------------
void VoiceADSRPanel::onAttackChanged(wxSpinEvent& event)
{
  m_sidDevice.setAttack(m_voice, event.GetPosition());
}

//------------------------------------------------------------------------------
void VoiceADSRPanel::onDecayChanged(wxSpinEvent& event)
{
  m_sidDevice.setDecay(m_voice, event.GetPosition());
}

//------------------------------------------------------------------------------
void VoiceADSRPanel::onSustainChanged(wxSpinEvent& event)
{
  m_sidDevice.setSustain(m_voice, event.GetPosition());
}

//------------------------------------------------------------------------------
void VoiceADSRPanel::onReleaseChanged(wxSpinEvent& event)
{
  m_sidDevice.setRelease(m_voice, event.GetPosition());
}

}       // namespace
