/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_SID_NULL_HPP_
#define _RANDOM_SID_SID_NULL_HPP_

#include "ISidDevice.hpp"

namespace r4nd0mSID {

class SidDeviceNull:
    public ISidDevice
{
public:
  SidDeviceNull();
  virtual ~SidDeviceNull();

  // ISidDevice
  virtual int init() override;
  virtual int setVolume(uint8_t volume) override;
  virtual int setVoiceFrequency(
      enum RSVoice_t voice, uint16_t frequency) override;
  virtual int setVoiceShape(enum RSVoice_t voice, uint8_t shape) override;
  virtual int startVoice(enum RSVoice_t voice) override;
  virtual int releaseVoice(enum RSVoice_t voice) override;
};

}       // namespace
#endif  // _RANDOM_SID_SID_NULL_HPP_
