/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SidDeviceNull.hpp"

#include <iostream>

namespace r4nd0mSID {

//------------------------------------------------------------------------------
SidDeviceNull::SidDeviceNull()
{
}

//------------------------------------------------------------------------------
SidDeviceNull::~SidDeviceNull()
{
}
//------------------------------------------------------------------------------
int SidDeviceNull::init()
{
  std::cout << "SID null init" << std::endl;

  return 0;
}

//------------------------------------------------------------------------------
int SidDeviceNull::setVolume(uint8_t volume)
{
  std::cout << "SID null set volume to " <<
      static_cast<int>(volume) << std::endl;

  return 0;
}

//------------------------------------------------------------------------------
int SidDeviceNull::setVoiceFrequency(enum RSVoice_t voice, uint16_t frequency)
{
  std::cout << "SID null set frequency " << frequency <<
      " on voice " << voice << std::endl;

  return 0;
}

//------------------------------------------------------------------------------
int SidDeviceNull::setVoiceShape(enum RSVoice_t voice, uint8_t shape)
{
  std::cout << "SID null set shape " << (int)shape <<
      " on voice " << voice << std::endl;

  return 0;
}

//------------------------------------------------------------------------------
int SidDeviceNull::startVoice(enum RSVoice_t voice)
{
  std::cout << "SID null start voice " << (int)voice << std::endl;

  return 0;
}

//------------------------------------------------------------------------------
int SidDeviceNull::releaseVoice(enum RSVoice_t voice)
{
  std::cout << "SID null release voice " << (int)voice << std::endl;

  return 0;
}

}       // namespace
