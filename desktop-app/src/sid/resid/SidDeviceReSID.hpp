/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_SID_DEVICE_RESID_HPP_
#define _RANDOM_SID_SID_DEVICE_RESID_HPP_

#include <memory>

#include <rslib/SID.h>

#include "../../sound/ISoundSource.hpp"
#include "../ISidDevice.hpp"
#include "SIDRegistersReSID.hpp"

namespace r4nd0mSID {

class SidDeviceReSID:
    public ISidDevice
{
public:
  SidDeviceReSID();
  virtual ~SidDeviceReSID();

  // ISidDevice
  virtual int init() override;
  virtual int setVolume(uint8_t volume) override;
  virtual int setVoiceFrequency(
      enum RSVoice_t voice, uint16_t frequency) override;
  virtual int setVoiceShape(enum RSVoice_t voice, uint8_t shape) override;
  virtual int startVoice(enum RSVoice_t voice) override;
  virtual int releaseVoice(enum RSVoice_t voice) override;
  virtual int setAttack(enum RSVoice_t voice, uint8_t attack) override;
  virtual int setDecay(enum RSVoice_t voice, uint8_t decay) override;
  virtual int setSustain(enum RSVoice_t voice, uint8_t sustain) override;
  virtual int setRelease(enum RSVoice_t voice, uint8_t release) override;
  virtual int setPulseWidth(enum RSVoice_t voice, uint16_t pulseWidth) override;
  virtual int enableRing(enum RSVoice_t voice, bool isEnabled) override;
  virtual int enableSync(enum RSVoice_t voice, bool isEnabled) override;

  // ISoundSource
  virtual void feedDevice(IAudioSink& device) override;

private:
  SID m_reSID;
  SIDRegistersReSID m_sidRegisters;
  RSSID_t m_rsSidDevice;
  int m_sidBufferSize;
  std::unique_ptr<char[]> m_sidBuffer;
  int m_sidBufferIndexFirst;
  int m_sidBufferIndexLast;

  void generate(IAudioSink& device);
  int emptyLocalBuffer(IAudioSink& device);
};

}       // namespace
#endif  // _RANDOM_SID_SID_DEVICE_RESID_HPP_
