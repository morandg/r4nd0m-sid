/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstring>

#include "SidDeviceReSID.hpp"

#include <iostream> // FIXME Logging

namespace r4nd0mSID {

//------------------------------------------------------------------------------
SidDeviceReSID::SidDeviceReSID():
    m_sidRegisters(m_reSID),
    m_sidBufferSize(2048),
    m_sidBuffer(new char[m_sidBufferSize]),
    m_sidBufferIndexFirst(0),
    m_sidBufferIndexLast(0)
{
  memset(m_sidBuffer.get(), 0, m_sidBufferSize);
  m_rsSidDevice = RSSID_create(m_sidRegisters.getCRegs());
}

//------------------------------------------------------------------------------
SidDeviceReSID::~SidDeviceReSID()
{
}
//------------------------------------------------------------------------------
int SidDeviceReSID::init()
{
  m_reSID.set_chip_model(MOS6581);
  m_reSID.reset();

  if(RSSID_init(m_rsSidDevice))
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
int SidDeviceReSID::setVolume(uint8_t volume)
{
  return RSSIDDev_setVolume(m_rsSidDevice, volume);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::setVoiceFrequency(enum RSVoice_t voice, uint16_t frequency)
{
  return RSSIDDev_setVoiceFrequency(m_rsSidDevice, voice, frequency);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::setVoiceShape(enum RSVoice_t voice, uint8_t shape)
{
  return RSSIDDev_setVoiceShape(m_rsSidDevice, voice, shape);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::startVoice(enum RSVoice_t voice)
{
  return RSSIDDev_startVoice(m_rsSidDevice, voice);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::releaseVoice(enum RSVoice_t voice)
{
  return RSSIDDev_releaseVoice(m_rsSidDevice, voice);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::setAttack(enum RSVoice_t voice, uint8_t attack)
{
  return RSSIDDev_setAttack(m_rsSidDevice, voice, attack);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::setDecay(enum RSVoice_t voice, uint8_t decay)
{
  return RSSIDDev_setDecay(m_rsSidDevice, voice, decay);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::setSustain(enum RSVoice_t voice, uint8_t sustain)
{
  return RSSIDDev_setSustain(m_rsSidDevice, voice, sustain);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::setRelease(enum RSVoice_t voice, uint8_t release)
{
  return RSSIDDev_setRelease(m_rsSidDevice, voice, release);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::setPulseWidth(enum RSVoice_t voice, uint16_t pulseWidth)
{
  return RSSIDDev_setPulseWidth(m_rsSidDevice, voice, pulseWidth);
}

//------------------------------------------------------------------------------
int SidDeviceReSID::enableRing(enum RSVoice_t voice, bool isEnabled)
{
  return RSSIDDev_enableRing(m_rsSidDevice, voice, isEnabled);
}

//------------------------------------------------------------------------------
int SidDeviceReSID:: enableSync(enum RSVoice_t voice, bool isEnabled)
{
  return RSSIDDev_enableSync(m_rsSidDevice, voice, isEnabled);
}

//------------------------------------------------------------------------------
void SidDeviceReSID::feedDevice(IAudioSink& device)
{
  generate(device);
}

//------------------------------------------------------------------------------
void SidDeviceReSID::generate(IAudioSink& device)
{
  while(!emptyLocalBuffer(device))
  {
    cycle_count delta = 10000;

    m_sidBufferIndexFirst = 0;
    m_sidBufferIndexLast =0;
    while(delta)
      m_sidBufferIndexLast += m_reSID.clock(
          delta, (short*)m_sidBuffer.get(), m_sidBufferSize);
  }
}

//------------------------------------------------------------------------------
int SidDeviceReSID::emptyLocalBuffer(IAudioSink& device)
{
  int ret;
  int bufferSize = m_sidBufferIndexLast - m_sidBufferIndexFirst;

  ret = device.write( m_sidBuffer.get() + m_sidBufferIndexFirst, bufferSize);
  if(ret < 0)
  {
    std::cerr << "Error sinking reSID audio: " << ret;
    return -1;
  }
  else if(ret == 0)
    return bufferSize;

  m_sidBufferIndexFirst += ret;

  return m_sidBufferIndexLast - m_sidBufferIndexFirst;
}


}       // namespace
