/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SIDRegisters.hpp"

extern "C"
{

//------------------------------------------------------------------------------
static int CBSIDRegs_init(void* ctx)
{
  r4nd0mSID::ISIDRegisters* regs = static_cast<r4nd0mSID::ISIDRegisters*>(ctx);

  return regs->init();
}

//------------------------------------------------------------------------------
static int CBSIDRegs_read(void* ctx, uint8_t reg, uint8_t* value)
{
  r4nd0mSID::ISIDRegisters* regs = static_cast<r4nd0mSID::ISIDRegisters*>(ctx);

  return regs->read(reg, *value);
}

//------------------------------------------------------------------------------
static int CBSIDRegs_write(void* ctx, uint8_t reg, uint8_t value)
{
  r4nd0mSID::ISIDRegisters* regs = static_cast<r4nd0mSID::ISIDRegisters*>(ctx);

  return regs->write(reg, value);
}

} // extern "C"

namespace r4nd0mSID {

//------------------------------------------------------------------------------
SIDRegisters::SIDRegisters()
{
  m_ctxt.init = CBSIDRegs_init;
  m_ctxt.readRegister = CBSIDRegs_read;
  m_ctxt.writeRegister = CBSIDRegs_write;
  m_ctxt.context = this;
}

//------------------------------------------------------------------------------
SIDRegisters::~SIDRegisters()
{
}

//------------------------------------------------------------------------------
RSISIDRegs_t SIDRegisters::getCRegs()
{
  return &m_ctxt;
}
}       // namespace
