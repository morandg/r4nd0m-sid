/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Application.hpp"
#include "WxApp.hpp"

namespace r4nd0mSID {

//------------------------------------------------------------------------------
WxApp::WxApp():
  m_mainWindow(nullptr)
{
}

//------------------------------------------------------------------------------
WxApp::~WxApp()
{
}

//------------------------------------------------------------------------------
bool WxApp::OnInit()
{
  if(Application::getInstance().init())
    return -1;

  m_mainWindow = new MainWindow("R4nd0m SID UI");
  if(m_mainWindow->init(Application::getInstance().getSidDevice()))
    return false;

  m_mainWindow->Show(true);

  return true;
}

}       // namespace
