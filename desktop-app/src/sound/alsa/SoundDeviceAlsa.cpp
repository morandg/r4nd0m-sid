/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>

#include "SoundDeviceAlsa.hpp"

extern "C" {

//------------------------------------------------------------------------------
static void alsaCCallback(snd_async_handler_t *pcm_callback)
{
  r4nd0mSID::SoundDeviceAlsa* alsaDev =
      static_cast<r4nd0mSID::SoundDeviceAlsa*>(snd_async_handler_get_callback_private(pcm_callback));

  alsaDev->onAlsaPcmCallback();
}

}   // extern "C"

namespace r4nd0mSID {

//------------------------------------------------------------------------------
SoundDeviceAlsa::SoundDeviceAlsa(ISoundSource& soundSource):
    m_soundSource(soundSource),
    m_pcm(nullptr)
{
}

//------------------------------------------------------------------------------
SoundDeviceAlsa::~SoundDeviceAlsa()
{
  close();
}

//------------------------------------------------------------------------------
void SoundDeviceAlsa::onAlsaPcmCallback()
{
  m_soundSource.feedDevice(*this);
}

//------------------------------------------------------------------------------
int SoundDeviceAlsa::init()
{
  std::string device = "default";
  snd_pcm_hw_params_t* hw_params;
  unsigned int sampleRate = 44100;
  snd_pcm_uframes_t buffer_size = 1024;
  snd_pcm_uframes_t period_size = 128;
  snd_async_handler_t *pcm_callback;
  unsigned int initialBufferSize = buffer_size;
  char* initialBuffer[initialBufferSize];
  int alsaErr;

  std::cout << "Sound device alsa init" << std::endl;

  alsaErr = snd_pcm_open(
      &m_pcm, "default", SND_PCM_STREAM_PLAYBACK, SND_PCM_ASYNC | SND_PCM_NONBLOCK);

  if(alsaErr < 0)
  {
    std::cerr << "Cannot open audio device: " <<
        snd_strerror(alsaErr) << std::endl;
    return -1;
  }

  alsaErr = snd_pcm_hw_params_malloc(&hw_params);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot allocate hw parameters device: " <<
        snd_strerror(alsaErr) << std::endl;
    return -1;
  }

  alsaErr = snd_pcm_hw_params_any(m_pcm, hw_params);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot create default hw param device: " <<
        snd_strerror(alsaErr) << std::endl;
    snd_pcm_hw_params_free(hw_params);
    return -1;
  }

  alsaErr = snd_pcm_hw_params_set_access(
      m_pcm, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot set access mod: " <<
        snd_strerror(alsaErr) << std::endl;
    snd_pcm_hw_params_free(hw_params);
    return -1;
  }

  alsaErr = snd_pcm_hw_params_set_format(
      m_pcm, hw_params, SND_PCM_FORMAT_S16_LE);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot set format: " <<
        snd_strerror(alsaErr) << std::endl;
    snd_pcm_hw_params_free(hw_params);
    return -1;
  }

  alsaErr = snd_pcm_hw_params_set_rate_near(
      m_pcm, hw_params, &sampleRate, NULL);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot set sample rate: " <<
        snd_strerror(alsaErr) << std::endl;
    snd_pcm_hw_params_free(hw_params);
    return -1;
  }

  alsaErr = snd_pcm_hw_params_set_channels(m_pcm, hw_params, 2);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot set channels: " <<
        snd_strerror(alsaErr) << std::endl;
    snd_pcm_hw_params_free(hw_params);
    return -1;
  }

  alsaErr = snd_pcm_hw_params_set_buffer_size_near(
      m_pcm, hw_params, &buffer_size);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot set buffer size: " <<
        snd_strerror(alsaErr) << std::endl;
    snd_pcm_hw_params_free(hw_params);
    return -1;
  }

  alsaErr = snd_pcm_hw_params_set_period_size_near(
      m_pcm, hw_params, &period_size, NULL);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot set period size: " <<
        snd_strerror(alsaErr) << std::endl;
    snd_pcm_hw_params_free(hw_params);
    return -1;
  }

  alsaErr = snd_pcm_hw_params(m_pcm, hw_params);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot apply HW params: " <<
        snd_strerror(alsaErr) << std::endl;
    snd_pcm_hw_params_free(hw_params);
    return -1;
  }

  snd_pcm_hw_params_free(hw_params);
  alsaErr = snd_pcm_prepare(m_pcm);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot prepare alsa device: " <<
        snd_strerror(alsaErr) << std::endl;
    snd_pcm_hw_params_free(hw_params);
    return -1;
  }
  memset(initialBuffer, 0, initialBufferSize);
  snd_pcm_writei(m_pcm, initialBuffer, initialBufferSize);

  alsaErr = snd_async_add_pcm_handler(
      &pcm_callback, m_pcm, alsaCCallback, this);
  if(alsaErr < 0)
  {
    std::cerr << "Cannot add PCM callback: " <<
        snd_strerror(alsaErr) << std::endl;
    return -1;
  }

  std::cout << "ALSA sound device ready" << std::endl;

  return 0;
}

//------------------------------------------------------------------------------
int SoundDeviceAlsa::write(char* buffer, int size)
{
  snd_pcm_sframes_t ret;


  ret = snd_pcm_writei(m_pcm, buffer, size);

  if(ret < 0)
  {
    if(ret == -EPIPE)
    {
      recoverXRun();
      return 0;
    }
    else if(errno == EAGAIN)
      return 0;

    std::cout << "Alsa write error " << snd_strerror(ret) << std::endl;
    return -1;
  }

  return ret;
}

//------------------------------------------------------------------------------
void SoundDeviceAlsa::close()
{
  std::cout << "Sound device alsa close" << std::endl;

  if(!m_pcm)
    return;

  snd_pcm_close(m_pcm);
  m_pcm = nullptr;
}

//------------------------------------------------------------------------------
void SoundDeviceAlsa::setSource(ISoundSource& input)
{
  std::cout << "Sound device alsa setSource" << std::endl;

  m_soundSource = input;
}

//------------------------------------------------------------------------------
void SoundDeviceAlsa::recoverXRun()
{
  int ret;
  static int xrunCount = 0;

  std::cout << "XRUN (" << ++xrunCount << ")" << std::endl;

  ret = snd_pcm_prepare(m_pcm);
  if(ret < 0)
  {
    std::cerr << "Cannot recover from alsa underrun: " <<
        snd_strerror(ret) << std::endl;
    return;
  }

  ret = snd_pcm_start(m_pcm);
  if(ret < 0)
  {
    std::cerr << "Cannot start PCM callback: " <<
        snd_strerror(ret) << std::endl;
    return;
  }
}

}       // namespace
