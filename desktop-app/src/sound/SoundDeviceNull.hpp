/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_SOUND_DEVICE_NULL_HPP_
#define _RANDOM_SID_SOUND_DEVICE_NULL_HPP_

#include "ISoundDevice.hpp"

namespace r4nd0mSID {

class SoundDeviceNull:
    public ISoundDevice
{
public:
  SoundDeviceNull();
  virtual ~SoundDeviceNull();

  virtual int init() override;
  virtual int write(char* buffer, int size) override;
  virtual void close() override;
  virtual void setSource(ISoundSource& input) override;
};

}       // namespace
#endif  // _RANDOM_SID_SOUND_DEVICE_NULL_HPP_

