/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>

#include "SoundDeviceNull.hpp"

namespace r4nd0mSID {

//------------------------------------------------------------------------------
SoundDeviceNull::SoundDeviceNull()
{
}

//------------------------------------------------------------------------------
SoundDeviceNull::~SoundDeviceNull()
{
}

//------------------------------------------------------------------------------
int SoundDeviceNull::init()
{
  std::cout << "Sound device null init" << std::endl;

  return 0;
}

//------------------------------------------------------------------------------
int SoundDeviceNull::write(char* buffer, int size)
{
  std::cout << "Sound device null write" << std::endl;

  return size;
}

//------------------------------------------------------------------------------
void SoundDeviceNull::close()
{
  std::cout << "Sound device null close" << std::endl;
}

//------------------------------------------------------------------------------
void SoundDeviceNull::setSource(ISoundSource& input)
{
  std::cout << "Sound device null setSource" << std::endl;

  // For testing
  input.feedDevice(*this);
}

}       // namespace

