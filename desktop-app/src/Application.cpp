/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <assert.h>

#ifdef _RS_HAS_ALSA_
#include "sound/alsa/SoundDeviceAlsa.hpp"
#else
#include "sound/SoundDeviceNull.hpp"
#endif

#include "sid/resid/SidDeviceReSID.hpp"

#include "Application.hpp"

namespace r4nd0mSID {

//------------------------------------------------------------------------------
Application::Application()
{
}

//------------------------------------------------------------------------------
Application::~Application()
{
}

//------------------------------------------------------------------------------
Application& Application::getInstance()
{
  static Application instance;

  return instance;
}

//------------------------------------------------------------------------------
int Application::init()
{
  m_sidDevice.reset(new SidDeviceReSID());
  if(m_sidDevice->init())
    return -1;

#ifdef _RS_HAS_ALSA_
  m_soundDevice.reset(new SoundDeviceAlsa(*m_sidDevice));
#else
  m_soundDevice.reset(new SoundDeviceNull());
#endif
  if(m_soundDevice->init())
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
ISidDevice& Application::getSidDevice()
{
  assert(m_sidDevice);

  return *m_sidDevice;
}

}       // namespac
