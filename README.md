My arduino project to control a Commodore 64 SID 658x using the MIDI
protocol.

# SID replacement
If you are unlucky and don't have a SID chip, you can use a
[swinsid](http://www.vintage-computers.it/swinsid-nano-v-2) as an
hardware replacement.

# Compilation
Everything is built using *make*, for more details, run:
```sh
make help
```