include ../common.mk

# Library variables
SOURCES = \
  src/ISID_regs.c \
  src/SID_regs_cached.c \
  src/SID.c
LIB_NAME          = rslib
BUILD_DIR         ?= ../build/rslib
LIB_NAME_STATIC   = $(LIB_NAME).a
OBJS              = $(patsubst src/%.c,$(BUILD_DIR)/%.o,$(SOURCES))
CFLAGS            += -Iinclude/
CXXFLAGS          += $(CFLAGS)

# Tests variables
TEST_SOURCES      = \
  tests/mocks/SID_regs_mock.cpp \
  tests/SIDTest.cpp \
  tests/SIDRegsCachedTest.cpp \
  tests/main.cpp

TEST_BUILD_DIR    = $(BUILD_DIR)/tests
TEST_APP          = $(BUILD_DIR)/$(LIB_NAME)_tests
TEST_CPP_OBJS     = $(patsubst tests/%.cpp,$(TEST_BUILD_DIR)/%.o,$(TEST_SOURCES))
TEST_C_OBJS       = $(patsubst src/%.c,$(TEST_BUILD_DIR)/%.o,$(SOURCES))
TEST_FLAGS        = -include $(CURDIR)/tests/CppUTestLeakDetection.h $(CPPUTEST_FLAGS)
TEST_LDFLAGS      = $(CPPUTEST_LDFLAGS)

DEFAULT_TARGET = lib
ifeq ($(HAS_CPPUTEST),1)
	DEFAULT_TARGET += tests
endif

# All depedencies
DEP               = $(OBJS:.o=.d) $(TEST_CPP_OBJS:.o=.d) $(TEST_C_OBJS:.o=.d)

##############################################################################################
# Default
##############################################################################################
all: $(DEFAULT_TARGET)
.PHONY: all

-include $(DEP)

##############################################################################################
# Library
##############################################################################################
lib: $(BUILD_DIR)/$(LIB_NAME_STATIC)
.PHONY: lib

$(BUILD_DIR)/%.o: src/%.c
	@printf $(PRINTF_CMD_FMT) "[ $(CC) ]"  "$@ ..."
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CC) $(CFLAGS) -c -MMD $< -o $@

$(BUILD_DIR)/$(LIB_NAME_STATIC): $(OBJS)
	@printf $(PRINTF_CMD_FMT) "[ $(AR) ]"  "$@ ..."
	$(SILENCE)$(AR) rcs $@ $(OBJS)

##############################################################################################
# Tests
##############################################################################################
tests: $(TEST_APP)
.PHONY: tests

$(TEST_BUILD_DIR)/%.o: tests/%.cpp
ifneq ($(HAS_CPPUTEST),1)
	$(error CppUTest not found, cannot build tests!)
endif
	@printf $(PRINTF_CMD_FMT) "[ $(CXX) ]"  "$@ ..."
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CXX) $(CXXFLAGS) $(TEST_FLAGS) -c -MMD $< -o $@

$(TEST_BUILD_DIR)/%.o: src/%.c
	@printf $(PRINTF_CMD_FMT) "[ $(CC) ]"  "$@ ..."
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(CC) $(CFLAGS) $(TEST_FLAGS) -c -MMD $< -o $@

$(TEST_APP): $(TEST_CPP_OBJS) $(TEST_C_OBJS)
	@printf $(PRINTF_CMD_FMT) "[ $(CXX) ]"  "$@ ..."
	$(SILENCE)$(CXX) $(TEST_CPP_OBJS) $(TEST_C_OBJS) $(LDFLAGS) $(TEST_LDFLAGS) -o $@
	@printf $(PRINTF_CMD_FMT) "[ RUN ]" $@
	$(SILENCE)./$(TEST_APP) #|| rm $(TEST_APP)

##############################################################################################
# Cleanup
##############################################################################################
clean:
	@printf $(PRINTF_CMD_FMT) "[ CLEAN ]"  "$(LIB_NAME)"
	$(SILENCE)rm -rf $(BUILD_DIR)
.PHONY: clean