/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <assert.h>

#include "../include/rslib/SID.h"
#include "../include/rslib/SID_regs_cached.h"
#include "SID_private.h"

#define getSID(x) ((RSSID*)x)

//------------------------------------------------------------------------------
static int voiceToCtrlRegister(enum RSVoice_t voice, uint8_t* ctrlReg)
{
  switch(voice)
  {
    case RS_VOICE_1:
      *ctrlReg = SID_REG_VOICE_1_CTRL;
      break;
    case RS_VOICE_2:
      *ctrlReg = SID_REG_VOICE_2_CTRL;
      break;
    case RS_VOICE_3:
      *ctrlReg = SID_REG_VOICE_3_CTRL;
      break;
    default:
      return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
static int voiceToAttackDecayRegister(enum RSVoice_t voice, uint8_t* reg)
{
  switch(voice)
  {
  case RS_VOICE_1:
    *reg = SID_REG_VOICE_1_ATK_DCY;
    break;
  case RS_VOICE_2:
    *reg = SID_REG_VOICE_2_ATK_DCY;
    break;
  case RS_VOICE_3:
    *reg = SID_REG_VOICE_3_ATK_DCY;
    break;
  default:
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
static int voiceToSustainReleaseRegister(enum RSVoice_t voice, uint8_t* reg)
{
  switch(voice)
  {
  case RS_VOICE_1:
    *reg = SID_REG_VOICE_1_STN_RIS;
    break;
  case RS_VOICE_2:
    *reg = SID_REG_VOICE_2_STN_RIS;
    break;
  case RS_VOICE_3:
    *reg = SID_REG_VOICE_3_STN_RIS;
    break;
  default:
    return -1;
  }

  return 0;
}

//------------------------------------------------------------------------------
RSSID_t RSSID_create(RSISIDRegs_t regs)
{
  RSSID* sid = (RSSID_t)malloc(sizeof(RSSID));

  if(!sid)
    return NULL;

  sid->regsCached = RSSIDRegsCached_create(regs);
  if(!sid->regsCached)
  {
    free(sid);
    return NULL;
  }

  return sid;
}

//------------------------------------------------------------------------------
void RSSID_destroy(RSSID_t self)
{
  assert(self != NULL);
  assert(getSID(self)->regsCached);

  free(getSID(self)->regsCached);
  free(self);
}

//------------------------------------------------------------------------------
int RSSID_init(RSSID_t self)
{
  assert(self);
  assert(getSID(self)->regsCached);

  return RSISIDRegs_init(getSID(self)->regsCached);
}

//------------------------------------------------------------------------------
int RSSIDDev_setVolume(RSSID_t self, uint8_t volume)
{
  uint8_t regValue;

  assert(self);
  assert(getSID(self)->regsCached);

  if(volume & 0xF0)
    return -1;

  if(RSISIDRegs_read(getSID(self)->regsCached, SID_REG_MODE_VOL, &regValue))
    return -1;

  regValue = (regValue & 0xF0) | volume;

  return RSISIDRegs_write(getSID(self)->regsCached, SID_REG_MODE_VOL, regValue);
}

//------------------------------------------------------------------------------
int RSSIDDev_setVoiceShape(RSSID_t self, enum RSVoice_t voice, uint8_t shape)
{
  uint8_t regValue;
  uint8_t ctrlReg;

  assert(self);
  assert(getSID(self)->regsCached);

  if((shape & 0x0f) ||
      voiceToCtrlRegister(voice, &ctrlReg))
    return -1;

  if(RSISIDRegs_read(getSID(self)->regsCached, ctrlReg, &regValue))
    return -1;

  regValue &= 0x0F;
  regValue |= shape;

  return RSISIDRegs_write(getSID(self)->regsCached, ctrlReg, regValue);
}

//------------------------------------------------------------------------------
int RSSIDDev_setVoiceFrequency(
    RSSID_t self, enum RSVoice_t voice, RSSIDFrequency frequency)
{
  uint8_t freqRegLow;
  uint8_t freqRegHigh;

  assert(self);
  assert(getSID(self)->regsCached);

  switch(voice)
  {
  case RS_VOICE_1:
    freqRegLow = SID_REG_VOICE_1_FREQ_LOW;
    freqRegHigh = SID_REG_VOICE_1_FREQ_HIGH;
    break;
  case RS_VOICE_2:
    freqRegLow = SID_REG_VOICE_2_FREQ_LOW;
    freqRegHigh = SID_REG_VOICE_2_FREQ_HIGH;
    break;
  case RS_VOICE_3:
    freqRegLow = SID_REG_VOICE_3_FREQ_LOW;
    freqRegHigh = SID_REG_VOICE_3_FREQ_HIGH;
    break;
  default:
    return -1;
  }

  if(RSISIDRegs_write(getSID(self)->regsCached, freqRegLow, frequency) ||
      RSISIDRegs_write(getSID(self)->regsCached, freqRegHigh, frequency >> 8))
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
int RSSIDDev_startVoice(RSSID_t self, enum RSVoice_t voice)
{
  uint8_t ctrlReg;
  uint8_t regValue;

  assert(self);
  assert(getSID(self)->regsCached);

  if(voiceToCtrlRegister(voice, &ctrlReg) ||
      RSISIDRegs_read(getSID(self)->regsCached, ctrlReg, &regValue))
    return -1;

  regValue |= SID_BIT_GATE;

  return RSISIDRegs_write(getSID(self)->regsCached, ctrlReg, regValue);
}

//------------------------------------------------------------------------------
int RSSIDDev_releaseVoice(RSSID_t self, enum RSVoice_t voice)
{
  uint8_t ctrlReg;
  uint8_t regValue;

  assert(self);
  assert(getSID(self)->regsCached);

  if(voiceToCtrlRegister(voice, &ctrlReg) ||
      RSISIDRegs_read(getSID(self)->regsCached, ctrlReg, &regValue))
    return -1;

  regValue &= ~SID_BIT_GATE;

  return RSISIDRegs_write(getSID(self)->regsCached, ctrlReg, regValue);
}

//------------------------------------------------------------------------------
int RSSIDDev_setAttack(RSSID_t self, enum RSVoice_t voice, uint8_t attack)
{
  uint8_t reg;
  uint8_t regValue;

  assert(self);
  assert(getSID(self)->regsCached);

  if(attack & 0xf0)
    return -1;

  if(voiceToAttackDecayRegister(voice, &reg))
    return -1;

  if(RSISIDRegs_read(getSID(self)->regsCached, reg, &regValue))
    return -1;

  regValue = (regValue & 0x0F) | (attack << 4);

  return RSISIDRegs_write(getSID(self)->regsCached, reg, regValue);
}

//------------------------------------------------------------------------------
int RSSIDDev_setDecay(RSSID_t self, enum RSVoice_t voice, uint8_t decay)
{
  uint8_t reg;
  uint8_t regValue;

  assert(self);
  assert(getSID(self)->regsCached);

  if(decay & 0xf0)
    return -1;

  if(voiceToAttackDecayRegister(voice, &reg))
    return -1;

  if(RSISIDRegs_read(getSID(self)->regsCached, reg, &regValue))
    return -1;

  regValue = (regValue & 0xF0) | decay;

  return RSISIDRegs_write(getSID(self)->regsCached, reg, regValue);
}

//------------------------------------------------------------------------------
int RSSIDDev_setSustain(RSSID_t self, enum RSVoice_t voice, uint8_t sustain)
{
  uint8_t reg;
  uint8_t regValue;

  assert(self);
  assert(getSID(self)->regsCached);

  if(sustain & 0xf0)
    return -1;

  if(voiceToSustainReleaseRegister(voice, &reg))
    return -1;

  if(RSISIDRegs_read(getSID(self)->regsCached, reg, &regValue))
    return -1;

  regValue = (regValue & 0x0F) | (sustain << 4);

  return RSISIDRegs_write(getSID(self)->regsCached, reg, regValue);
}

//------------------------------------------------------------------------------
int RSSIDDev_setRelease(RSSID_t self, enum RSVoice_t voice, uint8_t release)
{
  uint8_t reg;
  uint8_t regValue;

  assert(self);
  assert(getSID(self)->regsCached);

  if(release & 0xf0)
    return -1;

  if(voiceToSustainReleaseRegister(voice, &reg))
    return -1;

  if(RSISIDRegs_read(getSID(self)->regsCached, reg, &regValue))
    return -1;

  regValue = (regValue & 0xF0) | release;

  return RSISIDRegs_write(getSID(self)->regsCached, reg, regValue);
}

//------------------------------------------------------------------------------
int RSSIDDev_setPulseWidth(
    RSSID_t self, enum RSVoice_t voice, uint16_t pulseWidth)
{
  uint8_t regPwLow;
  uint8_t regPwHigh;

  if(pulseWidth & 0xF000)
    return -1;

  switch(voice)
  {
  case RS_VOICE_1:
    regPwLow = SID_REG_VOICE_1_PW_LOW;
    regPwHigh = SID_REG_VOICE_1_PW_HIGH;
    break;
  case RS_VOICE_2:
    regPwLow = SID_REG_VOICE_2_PW_LOW;
    regPwHigh = SID_REG_VOICE_2_PW_HIGH;
    break;
  case RS_VOICE_3:
    regPwLow = SID_REG_VOICE_3_PW_LOW;
    regPwHigh = SID_REG_VOICE_3_PW_HIGH;
    break;
  default:
    return -1;
  }

  if(RSISIDRegs_write(getSID(self)->regsCached, regPwLow, pulseWidth) ||
      RSISIDRegs_write(getSID(self)->regsCached, regPwHigh, pulseWidth >> 8))
    return -1;

  return 0;
}

//------------------------------------------------------------------------------
int RSSIDDev_enableRing(RSSID_t self, enum RSVoice_t voice, uint8_t isEnabled)
{
  uint8_t regValue;
  uint8_t reg;

  if(voiceToCtrlRegister(voice, &reg))
    return -1;

  if(RSISIDRegs_read(getSID(self)->regsCached, reg, &regValue))
    return -1;

  if(isEnabled)
  {
    if(regValue & SID_BIT_RING)
      return 0;

    regValue |= SID_BIT_RING;
  }
  if(!isEnabled)
  {
    if(!(regValue & SID_BIT_RING))
      return 0;

    regValue &= ~SID_BIT_RING;
  }


  return RSISIDRegs_write(getSID(self)->regsCached, reg, regValue);
}

//------------------------------------------------------------------------------
int RSSIDDev_enableSync(RSSID_t self, enum RSVoice_t voice, uint8_t isEnabled)
{
  uint8_t regValue;
  uint8_t reg;

  if(voiceToCtrlRegister(voice, &reg))
    return -1;

  if(RSISIDRegs_read(getSID(self)->regsCached, reg, &regValue))
    return -1;

  if(isEnabled)
  {
    if(regValue & SID_BIT_SYNC)
      return 0;

    regValue |= SID_BIT_SYNC;
  }
  if(!isEnabled)
  {
    if(!(regValue & SID_BIT_SYNC))
      return 0;

    regValue &= ~SID_BIT_SYNC;
  }

  return RSISIDRegs_write(getSID(self)->regsCached, reg, regValue);

//  uint8_t regValue;
//  uint8_t reg;
//
//  if(voiceToCtrlRegister(voice, &reg))
//    return -1;
//
//  if(RSISIDRegs_read(getSID(self)->regsCached, reg, &regValue))
//    return -1;
//
//  if(isEnabled)
//  {
//    if(regValue & SID_BIT_RING)
//      return 0;
//
//    regValue |= SID_BIT_RING;
//  }
//  if(!isEnabled)
//  {
//    if(!(regValue & SID_BIT_RING))
//      return 0;
//
//    regValue &= ~SID_BIT_RING;
//  }
//
//
//  return RSISIDRegs_write(getSID(self)->regsCached, reg, regValue);
}
