/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <string.h>

#include "../include/rslib/SID_regs_cached.h"
#include "ISID_regs_private.h"
#include "SID_regs_cached_private.h"

#define SID_REG_WO_LAST           (uint8_t)24
#define SID_REG_LAST              (uint8_t)28

#define getRegsCachedIf(x)  ((SIDRegsCached*)x)
#define getCache(x)         getRegsCachedIf(context)->regs_cache
#define getBus(x)           getRegsCachedIf(context)->sidBus

//------------------------------------------------------------------------------
static void destroy(void* context)
{
  free(context);
}

//------------------------------------------------------------------------------
static int init(void* context)
{
  if(RSISIDRegs_init(getBus(context)))
    return -1;

  memset(getCache(context), 0, sizeof(getCache(context)));
  for(uint8_t reg = 0 ; reg <= SID_REG_WO_LAST ; ++reg)
  {
    if(RSISIDRegs_write(getBus(context), reg, getCache(context)[reg]))
      return -1;
  }

  return 0;
}


//------------------------------------------------------------------------------
static int read(void* context, uint8_t reg, uint8_t* value)
{
  if(reg > SID_REG_LAST)
    return -1;

  if(reg <= SID_REG_WO_LAST)
  {
    *value = getCache(context)[reg];
    return 0;
  }

  return RSISIDRegs_read(getBus(context), reg, value);
}

//------------------------------------------------------------------------------
static int write(void* context, uint8_t reg, uint8_t value)
{
  if(reg > SID_REG_WO_LAST)
    return -1;

  if(RSISIDRegs_write(getBus(context), reg, value))
    return -1;

  getCache(context)[reg] = value;

  return 0;
}

//------------------------------------------------------------------------------
RSISIDRegs_t RSSIDRegsCached_create(RSISIDRegs_t sidBus)
{
  SIDRegsCached* me = (SIDRegsCached*)malloc(sizeof(SIDRegsCached));

  if(!sidBus)
    return NULL;

  memset(me, 0, sizeof(SIDRegsCached));
  me->iSIDRegs.destroy = destroy;
  me->iSIDRegs.init = init;
  me->iSIDRegs.readRegister = read;
  me->iSIDRegs.writeRegister = write;
  me->iSIDRegs.context = me;
  me->sidBus = sidBus;

  return &me->iSIDRegs;
}
