/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_LIB_SID_PRIVATE_H_
#define _RANDOM_SID_LIB_SID_PRIVATE_H_

#include "../include/rslib/ISID_regs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SID_REG_VOICE_1_FREQ_LOW  (uint8_t)0x00
#define SID_REG_VOICE_1_FREQ_HIGH (uint8_t)0x01
#define SID_REG_VOICE_1_PW_LOW    (uint8_t)0x02
#define SID_REG_VOICE_1_PW_HIGH   (uint8_t)0x03
#define SID_REG_VOICE_1_CTRL      (uint8_t)0x04
#define SID_REG_VOICE_1_ATK_DCY   (uint8_t)0x05
#define SID_REG_VOICE_1_STN_RIS   (uint8_t)0x06
#define SID_REG_VOICE_2_FREQ_LOW  (uint8_t)0x07
#define SID_REG_VOICE_2_FREQ_HIGH (uint8_t)0x08
#define SID_REG_VOICE_2_PW_LOW    (uint8_t)0x09
#define SID_REG_VOICE_2_PW_HIGH   (uint8_t)0x0A
#define SID_REG_VOICE_2_CTRL      (uint8_t)0x0B
#define SID_REG_VOICE_2_ATK_DCY   (uint8_t)0x0C
#define SID_REG_VOICE_2_STN_RIS   (uint8_t)0x0D
#define SID_REG_VOICE_3_FREQ_LOW  (uint8_t)0x0E
#define SID_REG_VOICE_3_FREQ_HIGH (uint8_t)0x0F
#define SID_REG_VOICE_3_PW_LOW    (uint8_t)0x10
#define SID_REG_VOICE_3_PW_HIGH   (uint8_t)0x11
#define SID_REG_VOICE_3_CTRL      (uint8_t)0x12
#define SID_REG_VOICE_3_ATK_DCY   (uint8_t)0x13
#define SID_REG_VOICE_3_STN_RIS   (uint8_t)0x14
#define SID_REG_MODE_VOL          (uint8_t)0x18

#define SID_BIT_GATE              (uint8_t)1
#define SID_BIT_SYNC              (uint8_t)(1 << 1)
#define SID_BIT_RING              (uint8_t)(1 << 2)

typedef struct _RSSID {
  RSISIDRegs_t regsCached;
} RSSID;

#ifdef __cplusplus
}   // extern "C"
#endif

#endif  // _RANDOM_SID_LIB_SID_PRIVATE_H_
