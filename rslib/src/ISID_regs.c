/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <assert.h>

#include "../include/rslib/ISID_regs.h"
#include "ISID_regs_private.h"

//------------------------------------------------------------------------------
void RSISIDRegs_destroy(RSISIDRegs_t self)
{
  assert(self);
  assert(getISIDRegisters(self)->destroy);

  getISIDRegisters(self)->destroy(getISIDRegisters(self)->context);
}

//------------------------------------------------------------------------------
int RSISIDRegs_init(RSISIDRegs_t self)
{
  assert(self);
  assert(getISIDRegisters(self)->init);

  return getISIDRegisters(self)->init(
      getISIDRegisters(self)->context);
}

//------------------------------------------------------------------------------
int RSISIDRegs_read(RSISIDRegs_t self, uint8_t reg, uint8_t* value)
{
  assert(self);
  assert(getISIDRegisters(self)->init);

  return getISIDRegisters(self)->readRegister
      (getISIDRegisters(self)->context, reg, value);
}

//------------------------------------------------------------------------------
int RSISIDRegs_write(RSISIDRegs_t self, uint8_t reg, uint8_t value)
{
  assert(self);
  assert(getISIDRegisters(self)->init);

  return getISIDRegisters(self)->writeRegister(
      getISIDRegisters(self)->context, reg, value);
}
