/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_LIB_ISID_REGS_PRIVATE_H_
#define _RANDOM_SID_LIB_ISID_REGS_PRIVATE_H_

#include "../include/rslib/ISID_regs.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _RSISIDRegisters{
  void (*destroy)(void* context);
  int (*init)(void* context);
  int (*readRegister)(void* context, uint8_t reg, uint8_t* value);
  int (*writeRegister)(void* context, uint8_t reg, uint8_t value);
  void* context;
} RSISIDRegigsters;

#define getISIDRegisters(x) ((RSISIDRegigsters*)x)

#ifdef __cplusplus
}   // extern "C"
#endif

#endif  // _RANDOM_SID_LIB_ISID_REGS_PRIVATE_H_
