/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_LIB_SID_REGS_CACHED_PRIVATE_H_
#define _RANDOM_SID_LIB_SID_REGS_CACHED_PRIVATE_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _SIDRegsCached {
  RSISIDRegigsters iSIDRegs;
  RSISIDRegs_t sidBus;
  uint8_t regs_cache[25];
} SIDRegsCached;

#ifdef __cplusplus
}   // extern "C"
#endif

#endif  // _RANDOM_SID_LIB_SID_REGS_CACHED_PRIVATE_H_
