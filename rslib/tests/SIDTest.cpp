/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <rslib/SID.h>

#include "mocks/SID_regs_mock.h"
#include "../src/ISID_regs_private.h"
#include "../src/SID_regs_cached_private.h"
#include "../src/SID_private.h"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

//------------------------------------------------------------------------------
TEST_GROUP(SID)
{
  TEST_SETUP()
  {
    m_isRegsMocked = false;
    RSSIDRegsMock_createStatic(&m_regs);
    m_sid = RSSID_create(&m_regs);
  }

  TEST_TEARDOWN()
  {
    if(m_isRegsMocked)
    {
      RSSID* rsSID = (RSSID*)getSID();
      rsSID->regsCached = m_cachedReg;
    }

    RSSID_destroy(m_sid);
    mock().clear();
  }

  RSISIDRegs_t getSIDRegs()
  {
    return &m_regs;
  }

  RSSID_t getSID()
  {
    return m_sid;
  }

  RSSID_t getSIDDirectRegsMock()
  {
    if(!m_isRegsMocked)
    {
      RSSID* rsSID = (RSSID*)getSID();
      m_cachedReg =  rsSID->regsCached;
      rsSID->regsCached = &m_regs;
      m_isRegsMocked = true;
    }

    return m_sid;
  }

private:
  RSISIDRegigsters m_regs;
  RSSID_t m_sid;
  bool m_isRegsMocked;
  RSISIDRegs_t m_cachedReg;
};

//------------------------------------------------------------------------------
TEST(SID, enableSyncVoice3)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_CTRL).
      withParameter("value", SID_BIT_SYNC).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableSync(sid, RS_VOICE_3, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableSyncVoice2)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_CTRL).
      withParameter("value", SID_BIT_SYNC).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableSync(sid, RS_VOICE_2, 1));

  mock().checkExpectations();
}

////------------------------------------------------------------------------------
TEST(SID, disableSyncVoice1Was0xFF)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0xFF & ~SID_BIT_SYNC).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableSync(sid, RS_VOICE_1, 0));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, disableSyncVoice1)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = SID_BIT_SYNC;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableSync(sid, RS_VOICE_1, 0));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, disableSyncVoice1AlreadyDisabled)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableSync(sid, RS_VOICE_1, 0));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableSyncVoice1AlreadyEnabled)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = SID_BIT_SYNC;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableSync(sid, RS_VOICE_1, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableSyncVoice1)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0x00;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", SID_BIT_SYNC).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableSync(sid, RS_VOICE_1, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableSyncVoice1CannotWriteRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0x00;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", SID_BIT_SYNC).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_enableSync(sid, RS_VOICE_1, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableSyncVoice1CannotReadRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0x00;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_enableSync(sid, RS_VOICE_1, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableSyncInvalidVoice)
{
  RSSID_t sid = getSID();

  CHECK_EQUAL(-1, RSSIDDev_enableSync(sid, RSVoice_t(5), 1));
}

//------------------------------------------------------------------------------
TEST(SID, enableRingVoice3)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_CTRL).
      withParameter("value", 0x04).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableRing(sid, RS_VOICE_3, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableRingVoice2)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_CTRL).
      withParameter("value", 0x04).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableRing(sid, RS_VOICE_2, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, disableRingVoice1Was0xFF)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0xFF & ~SID_BIT_RING).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableRing(sid, RS_VOICE_1, 0));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, disableRingVoice1)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = SID_BIT_RING;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableRing(sid, RS_VOICE_1, 0));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, disableRingVoice1AlreadyDisabled)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableRing(sid, RS_VOICE_1, 0));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableRingVoice1AlreadyEnabled)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = SID_BIT_RING;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableRing(sid, RS_VOICE_1, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableRingVoice1)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0x00;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0x04).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_enableRing(sid, RS_VOICE_1, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableRingVoice1CannotWriteRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0x00;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0x04).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_enableRing(sid, RS_VOICE_1, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableRingVoice1CannotReadRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0x00;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_enableRing(sid, RS_VOICE_1, 1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, enableRingInvalidVoice)
{
  RSSID_t sid = getSID();

  CHECK_EQUAL(-1, RSSIDDev_enableRing(sid, RSVoice_t(5), 1));
}

//------------------------------------------------------------------------------
TEST(SID, setPulsWidthVoice3)
{
  RSSID_t sid = getSID();
  uint16_t pulsWidth = 0x0ABC;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_PW_LOW).
      withParameter("value", 0xBC).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_PW_HIGH).
      withParameter("value", 0x0A).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setPulseWidth(sid, RS_VOICE_3, pulsWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulsWidthVoice2)
{
  RSSID_t sid = getSID();
  uint16_t pulsWidth = 0x0ABC;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_PW_LOW).
      withParameter("value", 0xBC).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_PW_HIGH).
      withParameter("value", 0x0A).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setPulseWidth(sid, RS_VOICE_2, pulsWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulsWidthVoice1)
{
  RSSID_t sid = getSID();
  uint16_t pulsWidth = 0x0ABC;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_PW_LOW).
      withParameter("value", 0xBC).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_PW_HIGH).
      withParameter("value", 0x0A).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setPulseWidth(sid, RS_VOICE_1, pulsWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulsWidthVoice1CannotWriteHigh)
{
  RSSID_t sid = getSID();
  uint16_t pulsWidth = 0x0ABC;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_PW_LOW).
      withParameter("value", 0xBC).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_PW_HIGH).
      withParameter("value", 0x0A).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setPulseWidth(sid, RS_VOICE_1, pulsWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulsWidthVoice1CannotWriteLow)
{
  RSSID_t sid = getSID();
  uint16_t pulsWidth = 0x0ABC;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_PW_LOW).
      withParameter("value", 0xBC).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setPulseWidth(sid, RS_VOICE_1, pulsWidth));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setPulsWidthVoice1InvalidVoice)
{
  RSSID_t sid = getSID();
  uint16_t pulsWidth = 0x0ABC;

  CHECK_EQUAL(-1, RSSIDDev_setPulseWidth(sid, (RSVoice_t)5, pulsWidth));
}

//------------------------------------------------------------------------------
TEST(SID, setPulsWidthVoice1InvalidPulse)
{
  RSSID_t sid = getSID();
  uint16_t pulsWidth = 0xF000;

  CHECK_EQUAL(-1, RSSIDDev_setPulseWidth(sid, RS_VOICE_1, pulsWidth));
}

//------------------------------------------------------------------------------
TEST(SID, setReleaseVoice3)
{
  RSSID_t sid = getSID();
  uint8_t release = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_STN_RIS).
      withParameter("value", 0x0B).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setRelease(sid, RS_VOICE_3, release));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setReleaseVoice2)
{
  RSSID_t sid = getSID();
  uint8_t release = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_STN_RIS).
      withParameter("value", 0x0B).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setRelease(sid, RS_VOICE_2, release));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setReleaseVoice1CannotWrite)
{
  RSSID_t sid = getSID();
  uint8_t release = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withParameter("value", 0x0B).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setRelease(sid, RS_VOICE_1, release));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setReleaseVoice1CannotReadRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;
  uint8_t release = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setRelease(sid, RS_VOICE_1, release));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setReleaseVoice1Was0xFF)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;
  uint8_t release = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withParameter("value", 0xFB).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setRelease(sid, RS_VOICE_1, release));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setReleaseVoice1)
{
  RSSID_t sid = getSID();
  uint8_t release = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withParameter("value", 0x0B).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setRelease(sid, RS_VOICE_1, release));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setReleaseInvalidRelease)
{
  RSSID_t sid = getSID();
  uint8_t release = 0x1B;

  CHECK_EQUAL(-1, RSSIDDev_setRelease(sid, RS_VOICE_1, release));
}

//------------------------------------------------------------------------------
TEST(SID, setReleaseInvalidVoice)
{
  RSSID_t sid = getSID();
  uint8_t release = 0x0B;

  CHECK_EQUAL(-1, RSSIDDev_setRelease(sid, (RSVoice_t)4, release));
}

//------------------------------------------------------------------------------
TEST(SID, setSustainVoice3)
{
  RSSID_t sid = getSID();
  uint8_t sustain = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_STN_RIS).
      withParameter("value", 0xB0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setSustain(sid, RS_VOICE_3, sustain));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainVoice2)
{
  RSSID_t sid = getSID();
  uint8_t sustain = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_STN_RIS).
      withParameter("value", 0xB0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setSustain(sid, RS_VOICE_2, sustain));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainVoice1CannotWrite)
{
  RSSID_t sid = getSID();
  uint8_t sustain = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withParameter("value", 0xB0).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setSustain(sid, RS_VOICE_1, sustain));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainVoice1CannotReadRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;
  uint8_t sustain = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setSustain(sid, RS_VOICE_1, sustain));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainVoice1Was0xFF)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;
  uint8_t sustain = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withParameter("value", 0xBF).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setSustain(sid, RS_VOICE_1, sustain));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainVoice1)
{
  RSSID_t sid = getSID();
  uint8_t sustain = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_STN_RIS).
      withParameter("value", 0xB0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setSustain(sid, RS_VOICE_1, sustain));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setSustainInvalidDecay)
{
  RSSID_t sid = getSID();
  uint8_t sustain = 0x1B;

  CHECK_EQUAL(-1, RSSIDDev_setSustain(sid, RS_VOICE_1, sustain));
}

//------------------------------------------------------------------------------
TEST(SID, setSustainInvalidVoice)
{
  RSSID_t sid = getSID();
  uint8_t sustain = 0x0B;

  CHECK_EQUAL(-1, RSSIDDev_setSustain(sid, (RSVoice_t)4, sustain));
}

//------------------------------------------------------------------------------
TEST(SID, setDecayVoice3)
{
  RSSID_t sid = getSID();
  uint8_t decay = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_ATK_DCY).
      withParameter("value", 0x0B).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setDecay(sid, RS_VOICE_3, decay));

  mock().checkExpectations();
}


//------------------------------------------------------------------------------
TEST(SID, setDecayVoice2)
{
  RSSID_t sid = getSID();
  uint8_t decay = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_ATK_DCY).
      withParameter("value", 0x0B).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setDecay(sid, RS_VOICE_2, decay));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setDecayVoice1CannotWrite)
{
  RSSID_t sid = getSID();
  uint8_t decay = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withParameter("value", 0x0B).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setDecay(sid, RS_VOICE_1, decay));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setDecayVoice1CannotReadRegster)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;
  uint8_t decay = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setDecay(sid, RS_VOICE_1, decay));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setDecayVoice1Was0xFF)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;
  uint8_t decay = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withParameter("value", 0xFB).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setDecay(sid, RS_VOICE_1, decay));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setDecayVoice1)
{
  RSSID_t sid = getSID();
  uint8_t decay = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withParameter("value", 0x0B).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setDecay(sid, RS_VOICE_1, decay));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setDecayInvalidDecay)
{
  RSSID_t sid = getSID();
  uint8_t delay = 0x1B;

  CHECK_EQUAL(-1, RSSIDDev_setDecay(sid, RS_VOICE_1, delay));
}

//------------------------------------------------------------------------------
TEST(SID, setDecayInvalidVoice)
{
  RSSID_t sid = getSID();
  uint8_t decay = 0x0B;

  CHECK_EQUAL(-1, RSSIDDev_setDecay(sid, (RSVoice_t)4, decay));
}

//------------------------------------------------------------------------------
TEST(SID, setAttackVoice3)
{
  RSSID_t sid = getSID();
  uint8_t attack = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_ATK_DCY).
      withParameter("value", 0xB0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setAttack(sid, RS_VOICE_3, attack));

  mock().checkExpectations();
}


//------------------------------------------------------------------------------
TEST(SID, setAttackVoice2)
{
  RSSID_t sid = getSID();
  uint8_t attack = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_ATK_DCY).
      withParameter("value", 0xB0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setAttack(sid, RS_VOICE_2, attack));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setAttackVoice1CannotWrite)
{
  RSSID_t sid = getSID();
  uint8_t attack = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withParameter("value", 0xB0).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setAttack(sid, RS_VOICE_1, attack));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setAttackVoice1CannotReadRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xff;
  uint8_t attack = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setAttack(sid, RS_VOICE_1, attack));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setAttackVoice1Was0xFF)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xff;
  uint8_t attack = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withParameter("value", 0xBF).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setAttack(sid, RS_VOICE_1, attack));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setAttackVoice1)
{
  RSSID_t sid = getSID();
  uint8_t attack = 0x0B;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_ATK_DCY).
      withParameter("value", 0xB0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setAttack(sid, RS_VOICE_1, attack));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setAttackInvalidAttack)
{
  RSSID_t sid = getSID();
  uint8_t attack = 0x1B;

  CHECK_EQUAL(-1, RSSIDDev_setAttack(sid, RS_VOICE_1, attack));
}

//------------------------------------------------------------------------------
TEST(SID, setAttackInvalidVoice)
{
  RSSID_t sid = getSID();
  uint8_t attack = 0x0B;

  CHECK_EQUAL(-1, RSSIDDev_setAttack(sid, (RSVoice_t)4, attack));
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice3)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = SID_BIT_GATE;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_CTRL).
      withParameter("value", 0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_releaseVoice(sid, RS_VOICE_3));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice2)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = SID_BIT_GATE;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_CTRL).
      withParameter("value", 0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_releaseVoice(sid, RS_VOICE_2));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice1RegValueFE)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", regValue & ~SID_BIT_GATE).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_releaseVoice(sid, RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice1)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = SID_BIT_GATE;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_releaseVoice(sid, RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice1CannotWriteRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = SID_BIT_GATE;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_releaseVoice(sid, RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoice1CannotReadRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = SID_BIT_GATE;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_releaseVoice(sid, RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, releaseVoiceInvalidVoice)
{
  RSSID_t sid = getSID();

  CHECK_EQUAL(-1, RSSIDDev_releaseVoice(sid, (RSVoice_t)0xff));
}

//------------------------------------------------------------------------------
TEST(SID, startVoice3)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_CTRL).
      withParameter("value", SID_BIT_GATE).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_startVoice(sid, RS_VOICE_3));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice2)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_CTRL).
      withParameter("value", SID_BIT_GATE).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_startVoice(sid, RS_VOICE_2));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice1RegValueFE)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFE;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", regValue | SID_BIT_GATE).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_startVoice(sid, RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice1CannotReadRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFE;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_startVoice(sid, RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice1)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", SID_BIT_GATE).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_startVoice(sid, RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoice1CannotWriteRegister)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", SID_BIT_GATE).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_startVoice(sid, RS_VOICE_1));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, startVoiceInvalidVoice)
{
  RSSID_t sid = getSID();

  CHECK_EQUAL(-1, RSSIDDev_startVoice(sid, (RSVoice_t)0xff));
}

//------------------------------------------------------------------------------
TEST(SID, setVoice3FrequencyABCD)
{
  RSSID_t sid = getSID();
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_FREQ_LOW).
      withParameter("value", 0xCD).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_FREQ_HIGH).
      withParameter("value", 0xAB).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setVoiceFrequency(sid, RS_VOICE_3, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoice2FrequencyABCD)
{
  RSSID_t sid = getSID();
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_FREQ_LOW).
      withParameter("value", 0xCD).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_FREQ_HIGH).
      withParameter("value", 0xAB).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setVoiceFrequency(sid, RS_VOICE_2, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoice1FrequencyABCD)
{
  RSSID_t sid = getSID();
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_FREQ_LOW).
      withParameter("value", 0xCD).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_FREQ_HIGH).
      withParameter("value", 0xAB).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setVoiceFrequency(sid, RS_VOICE_1, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoice1FrequencyABCDCannotSetHighFreq)
{
  RSSID_t sid = getSID();
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_FREQ_LOW).
      withParameter("value", 0xCD).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_FREQ_HIGH).
      withParameter("value", 0xAB).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setVoiceFrequency(sid, RS_VOICE_1, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoice1FrequencyABCDCannotSetLowFreq)
{
  RSSID_t sid = getSID();
  RSSIDFrequency frequency = 0xABCD;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_FREQ_LOW).
      withParameter("value", 0xCD).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setVoiceFrequency(sid, RS_VOICE_1, frequency));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVoiceFrequencyInvalidVoice)
{
  RSSID_t sid = getSID();
  RSSIDFrequency frequency = 0x0;

  CHECK_EQUAL(-1, RSSIDDev_setVoiceFrequency(sid, (RSVoice_t)0xFF, frequency));
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeSuccessRegister0xF0)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;
  uint8_t volume = 0x0f;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_MODE_VOL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_MODE_VOL).
      withParameter("value", regValue | volume).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setVolume(sid, volume));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeCannotReadRegister)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;
  uint8_t volume = 0x0f;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_MODE_VOL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setVolume(sid, volume));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeSuccess)
{
  RSSID_t sid = getSID();
  uint8_t volume = 0x01;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_MODE_VOL).
      withParameter("value", 0x01).
      andReturnValue(0);

  CHECK_EQUAL(0, RSSIDDev_setVolume(sid, volume));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeCannotWriteRegister)
{
  RSSID_t sid = getSID();
  uint8_t volume = 0x01;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_MODE_VOL).
      withParameter("value", 0x01).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSSIDDev_setVolume(sid, volume));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setVolumeInvalidVolume)
{
  RSSID_t sid = getSID();
  uint8_t volume = 0x10;

  CHECK_EQUAL(-1, RSSIDDev_setVolume(sid, volume));
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice3Noise)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_3_CTRL).
      withParameter("value", 0x80).
      andReturnValue(0);

  CHECK_EQUAL(
      0,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_3, RS_SHAPE_NOISE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice2Noise)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_2_CTRL).
      withParameter("value", 0x80).
      andReturnValue(0);

  CHECK_EQUAL(
      0,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_2, RS_SHAPE_NOISE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1Noise)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0x80).
      andReturnValue(0);

  CHECK_EQUAL(
      0,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_1, RS_SHAPE_NOISE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1Pulse)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0x40).
      andReturnValue(0);

  CHECK_EQUAL(
      0,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_1, RS_SHAPE_PULSE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1Saw)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0x20).
      andReturnValue(0);

  CHECK_EQUAL(
      0,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_1, RS_SHAPE_SAW));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1TriangleRegister0xFF)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0x1F).
      andReturnValue(0);

  CHECK_EQUAL(
      0,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_1, RS_SHAPE_TRIANGLE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1TriangleCannotReadRegisters)
{
  RSSID_t sid = getSIDDirectRegsMock();
  uint8_t regValue = 0xFF;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withOutputParameterReturning("value", &regValue, sizeof(regValue)).
      andReturnValue(-1);

  CHECK_EQUAL(
      -1,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_1, RS_SHAPE_TRIANGLE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1Triangle)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0x10).
      andReturnValue(0);

  CHECK_EQUAL(
      0,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_1, RS_SHAPE_TRIANGLE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1CannotWriteRegister)
{
  RSSID_t sid = getSID();

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(getSIDRegs()).
      withParameter("reg", SID_REG_VOICE_1_CTRL).
      withParameter("value", 0x10).
      andReturnValue(-1);

  CHECK_EQUAL(
      -1,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_1, RS_SHAPE_TRIANGLE));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, setShapeVoice1InvalidShape)
{
  RSSID_t sid = getSID();

  CHECK_EQUAL(
      -1,
      RSSIDDev_setVoiceShape(sid, RS_VOICE_1, 1));
}

//------------------------------------------------------------------------------
TEST(SID, setShapeInvalidVoice)
{
  RSSID_t sid = getSID();

  CHECK_EQUAL(
      -1,
      RSSIDDev_setVoiceShape(sid, (RSVoice_t)12, RS_SHAPE_TRIANGLE));
}

//------------------------------------------------------------------------------
TEST(SID, initFails)
{
  int retVal = -1;
  RSSID_t sid = getSID();

  mock().expectOneCall("RSSIDRegsMock_init").
      onObject(getSIDRegs()).
      andReturnValue(retVal);

  CHECK_EQUAL(retVal, RSSID_init(sid));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, initSuccess)
{
  int retVal = 0;
  RSSID_t sid = getSID();

  mock().expectOneCall("RSSIDRegsMock_init").
      onObject(getSIDRegs()).
      andReturnValue(retVal);

  for(uint8_t reg = 0 ; reg <= 0x18 ; ++reg)
  {
    mock().expectOneCall("RSISIDRegsMock_write").
        onObject(getSIDRegs()).
        withParameter("reg", reg).
        withParameter("value", 0).
        andReturnValue(0);
  }

  CHECK_EQUAL(retVal, RSSID_init(sid));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SID, createDeleteNoLeak)
{
  RSSID_t sid = getSID();

  CHECK(sid != NULL);
}
