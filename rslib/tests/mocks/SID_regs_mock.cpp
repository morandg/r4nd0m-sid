/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <CppUTestExt/MockSupport.h>

#include "../../src/ISID_regs_private.h"
#include "SID_regs_mock.h"

//------------------------------------------------------------------------------
void RSSIDRegsMock_createStatic(RSISIDRegs_t self)
{
  getISIDRegisters(self)->init = RSSIDRegsMock_init;
  getISIDRegisters(self)->readRegister = RSISIDRegsMock_read;
  getISIDRegisters(self)->writeRegister = RSISIDRegsMock_write;
  getISIDRegisters(self)->context = self;
}

//------------------------------------------------------------------------------
int RSSIDRegsMock_init(RSISIDRegs_t self)
{
  return mock().actualCall(__func__).
      onObject(self).
      returnIntValue();
}

//------------------------------------------------------------------------------
int RSISIDRegsMock_read(RSISIDRegs_t self, uint8_t reg, uint8_t* value)
{
  return mock().actualCall(__func__).onObject(self).
      withParameter("reg", reg).
      withOutputParameter("value", value).
      returnIntValue();
}

//------------------------------------------------------------------------------
int RSISIDRegsMock_write(RSISIDRegs_t self, uint8_t reg, uint8_t value)
{
  return mock().actualCall(__func__).onObject(self).
      withParameter("reg", reg).
      withParameter("value", value).
      returnIntValue();
}
