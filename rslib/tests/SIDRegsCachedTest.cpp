/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "mocks/SID_regs_mock.h"
#include "../include/rslib/SID_regs_cached.h"
#include "../src/ISID_regs_private.h"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

//------------------------------------------------------------------------------
TEST_GROUP(SIDRegsCached)
{
  TEST_SETUP()
  {
    RSSIDRegsMock_createStatic(&m_regsMock);
    m_regs = RSSIDRegsCached_create(regSIDRegsMock());
  }

  TEST_TEARDOWN()
  {
    RSISIDRegs_destroy(m_regs);
    mock().clear();
  }

  RSISIDRegs_t getSIDRegs()
  {
    return m_regs;
  }

  RSISIDRegs_t regSIDRegsMock()
  {
    return &m_regsMock;
  }

private:
  RSISIDRegigsters m_regsMock;
  RSISIDRegs_t m_regs;
};

//------------------------------------------------------------------------------
TEST(SIDRegsCached, writeRegisterRO)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t value = 0xAB;
  uint8_t reg = 25;

  CHECK_EQUAL(-1, RSISIDRegs_write(regs, reg, value));
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, readCacheAfterWrite)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t reg = 24;
  uint8_t value = 0x12;
  uint8_t readValue = 0xff;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(regSIDRegsMock()).
      withParameter("reg", reg).
      withParameter("value", value).
      andReturnValue(0);

  CHECK_EQUAL(0, RSISIDRegs_write(regs, reg, value));

  mock().checkExpectations();

  CHECK_EQUAL(0, RSISIDRegs_read(regs, reg, &readValue));
  CHECK_EQUAL(value, readValue);
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, writeRegisterWoLast)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t reg = 24;
  uint8_t value = 0x12;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(regSIDRegsMock()).
      withParameter("reg", reg).
      withParameter("value", value).
      andReturnValue(0);

  CHECK_EQUAL(0, RSISIDRegs_write(regs, reg, value));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, writeRegisterWoLastWriteFails)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t reg = 24;
  uint8_t value = 0x12;

  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(regSIDRegsMock()).
      withParameter("reg", reg).
      withParameter("value", value).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSISIDRegs_write(regs, reg, value));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, writeRegisterOutOfRange)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t value = 0x12;

  CHECK_EQUAL(-1, RSISIDRegs_write(regs, 29, value));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, readRegisterRoLast)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t actualValue = 0xAB;
  uint8_t readValue = 4;
  uint8_t reg = 28;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(regSIDRegsMock()).
      withParameter("reg", reg).
      withOutputParameterReturning("value", &actualValue, sizeof(actualValue)).
      andReturnValue(0);

  CHECK_EQUAL(0, RSISIDRegs_read(regs, reg, &readValue));

  mock().checkExpectations();

  CHECK_EQUAL(actualValue, readValue);
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, readRegisterRoFirst)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t actualValue = 0xAB;
  uint8_t readValue = 4;
  uint8_t reg = 25;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(regSIDRegsMock()).
      withParameter("reg", reg).
      withOutputParameterReturning("value", &actualValue, sizeof(actualValue)).
      andReturnValue(0);

  CHECK_EQUAL(0, RSISIDRegs_read(regs, reg, &readValue));

  mock().checkExpectations();

  CHECK_EQUAL(actualValue, readValue);
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, readRegisterRoFirstReadRegFails)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t actualValue = 0xAB;
  uint8_t readValue = 4;
  uint8_t reg = 25;

  mock().expectOneCall("RSISIDRegsMock_read").
      onObject(regSIDRegsMock()).
      withParameter("reg", reg).
      withOutputParameterReturning("value", &actualValue, sizeof(actualValue)).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSISIDRegs_read(regs, reg, &readValue));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, readRegisterWoLast)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t readValue = 4;
  uint8_t reg = 24;

  CHECK_EQUAL(0, RSISIDRegs_read(regs, reg, &readValue));
  CHECK_EQUAL(0, readValue);
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, readRegisterOutOfRange)
{
  RSISIDRegs_t regs = getSIDRegs();
  uint8_t value;

  CHECK_EQUAL(-1, RSISIDRegs_read(regs, 29, &value));
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, initUpdateAllRegistersSucceed)
{
  RSISIDRegs_t regs = getSIDRegs();

  mock().expectOneCall("RSSIDRegsMock_init").
      onObject(regSIDRegsMock()).
      andReturnValue(0);

  for(uint8_t reg = 0 ; reg <= 0x18 ; ++reg)
  {
    mock().expectOneCall("RSISIDRegsMock_write").
        onObject(regSIDRegsMock()).
        withParameter("reg", reg).
        withParameter("value", 0).
        andReturnValue(0);
  }

  CHECK_EQUAL(0, RSISIDRegs_init(regs));

  mock().checkExpectations();
}


//------------------------------------------------------------------------------
TEST(SIDRegsCached, initUpdateFirstRegisterFails)
{
  RSISIDRegs_t regs = getSIDRegs();
  RSISIDRegs_t regsMock = regSIDRegsMock();

  (void)regsMock;

  mock().expectOneCall("RSSIDRegsMock_init").
      onObject(regSIDRegsMock()).
      andReturnValue(0);
  mock().expectOneCall("RSISIDRegsMock_write").
      onObject(regSIDRegsMock()).
      withParameter("reg", 0).
      withParameter("value", 0).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSISIDRegs_init(regs));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, initInitRegsFails)
{
  RSISIDRegs_t regs = getSIDRegs();

  mock().expectOneCall("RSSIDRegsMock_init").
      onObject(regSIDRegsMock()).
      andReturnValue(-1);

  CHECK_EQUAL(-1, RSISIDRegs_init(regs));

  mock().checkExpectations();
}

//------------------------------------------------------------------------------
TEST(SIDRegsCached, createNoLeak)
{
  RSISIDRegs_t regs = getSIDRegs();

  CHECK(regs != NULL);
}
