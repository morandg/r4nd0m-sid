/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_LIB_SID_H_
#define _RANDOM_SID_LIB_SID_H_

#include "SIDDefs.h"
#include "ISID_regs.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void* RSSID_t;

RSSID_t RSSID_create(RSISIDRegs_t regs);
void RSSID_destroy(RSSID_t self);
int RSSID_init(RSSID_t self);
int RSSIDDev_setVolume(RSSID_t self, uint8_t volume);
int RSSIDDev_setVoiceShape(RSSID_t self, enum RSVoice_t voice, uint8_t shape);
int RSSIDDev_setVoiceFrequency(
    RSSID_t self, enum RSVoice_t voice, RSSIDFrequency frequency);
int RSSIDDev_startVoice(RSSID_t self, enum RSVoice_t voice);
int RSSIDDev_releaseVoice(RSSID_t self, enum RSVoice_t voice);
int RSSIDDev_setAttack(RSSID_t self, enum RSVoice_t voice, uint8_t attack);
int RSSIDDev_setDecay(RSSID_t self, enum RSVoice_t voice, uint8_t decay);
int RSSIDDev_setSustain(RSSID_t self, enum RSVoice_t voice, uint8_t sustain);
int RSSIDDev_setRelease(RSSID_t self, enum RSVoice_t voice, uint8_t release);
int RSSIDDev_setPulseWidth(
    RSSID_t self, enum RSVoice_t voice, uint16_t pulseWidth);
int RSSIDDev_enableRing(RSSID_t self, enum RSVoice_t voice, uint8_t isEnabled);
int RSSIDDev_enableSync(RSSID_t self, enum RSVoice_t voice, uint8_t isEnabled);

// Filter

#ifdef __cplusplus
}   // extern "C"
#endif

#endif  // _RANDOM_SID_LIB_SID_H_
