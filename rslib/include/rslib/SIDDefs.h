/**
 * R4nd0m SID a C64 SID interface
 *
 * Copyright (C) 2018 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _RANDOM_SID_SID_DEFS_H_
#define _RANDOM_SID_SID_DEFS_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

enum RSVoice_t {
  RS_VOICE_1        = 0,
  RS_VOICE_2        = 1,
  RS_VOICE_3        = 2
};

enum RSShape_t {
  RS_SHAPE_TRIANGLE = 1 << 4,
  RS_SHAPE_SAW      = 1 << 5,
  RS_SHAPE_PULSE    = 1 << 6,
  RS_SHAPE_NOISE    = 1 << 7
};

typedef uint16_t RSSIDFrequency;

#define RS_MAX_SID_FREQUENCY  (RSSIDFrequency)0xFFFF
#define RS_MAX_SID_PW         (uint16_t)0x0FFF
#define RS_MAX_SID_ADSR       (uint8_t)0x0F

#ifdef __cplusplus
}   // extern "C"
#endif

#endif  // _RANDOM_SID_SID_DEFS_H_
