ifneq ($(DEBUG),)
  CFLAGS     += -g
  CXXFLAGS   += -g
else
  CFLAGS     += -DNDEBUG
  CXXFLAGS   += -DNDEBUG
endif