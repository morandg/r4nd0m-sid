include common.mk

DEFAULT_TARGET   = desktop_app

ifeq ($(HAS_CPPUTEST),1)
  DEFAULT_TARGET += rslib_tests
endif

ifeq ($(HAS_AVR_CC),1)
  DEFAULT_TARGET += arduino
endif

all: $(DEFAULT_TARGET)
.PHONY: all

help:
	@echo "all            Build everything"
	@echo "rslib_tests    Build common lib tests"
	@echo "desktop_app    Build the desktop application"
	@echo "clean          Clean everything"
	@echo ""
	@echo "variables:"
	@echo "V              Set to any value to compile in verbose mode"
	@echo "DEBUG          Set to any value to enable debug symbols"
	@echo "CPPUTEST_HOME  Path to CppUTest home"
	@echo "HAS_ALSA       Set to 0 to disable alsa support"

.PHONY: help

rslib_tests:
	@$(MAKE) --no-print-directory -C rslib tests
.PHONY: rslib_tests

arduino:
	@$(MAKE) --no-print-directory -C arduino
.PHONY: arduino

desktop_app:
	@$(MAKE) --no-print-directory -C desktop-app/
.PHONY:desktop-app

clean:
	@$(MAKE) clean --no-print-directory -C arduino/
	@$(MAKE) clean --no-print-directory -C desktop-app/
	@$(MAKE) clean --no-print-directory -C rslib/
	@$(MAKE) clean --no-print-directory -C resid/
.PHONY: clean